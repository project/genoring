GenoRing
********

To be filled...

===============================


CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------


REQUIREMENTS
------------

This module has no specific requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

MAINTAINERS
-----------

Current maintainers:
  * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv
