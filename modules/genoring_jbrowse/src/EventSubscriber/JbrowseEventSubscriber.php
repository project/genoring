<?php

namespace Drupal\genoring_jbrowse\EventSubscriber;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\genoring\Event\DataModelManagementEvent;
use Drupal\genoring\Event\GenoringEvents;
use Drupal\menu_link_content\Entity\MenuLinkContent;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * GenoRing event subscriber to handle default behaviors.
 */
class JbrowseEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Constructs a GenoringEventsSubscriber object.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(TranslationInterface $string_translation) {
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[GenoringEvents::DATA_MODEL_MANAGEMENT][] = 'jbrowseDataModelManagement';
    return $events;
  }

  /**
   * Provides default action to available data models.
   *
   * @param \Drupal\genoring\Event\DataModelManagementEvent $event
   *   The event.
   */
  public function jbrowseDataModelManagement(DataModelManagementEvent $event) {
    // @todo Implement.
    $data_model = $event->getDataModel();
    $management_form = $event->getManagementForm();
    // JBrowse supports the 'genome' data model.
    if ('__genome' == strstr($data_model->id(), '__')) {
      // Get all genomes.
      $genome_storage = \Drupal::entityTypeManager()->getStorage($data_model->id());
      $genome_ids = $genome_storage->getQuery()->accessCheck(FALSE)->execute();
      $genomes = $genome_storage->loadMultiple($genome_ids);
      $genome_files = [];
      foreach ($genomes as $genome) {
        $genome_files[] = [
          'fastas' => array_map(
            function ($item) {
              return $item['value'] ?? '';
            },
            $genome->get('genoring_genome_fasta')->getValue()
          ),
          'gff3s' => array_map(
            function ($item) {
              return $item['value'] ?? '';
            },
            $genome->get('genoring_genome_gff3')->getValue()
          ),
        ];
      }
      // Currently, we only have one genome. But in the future, we may want to
      // load multiple genomes or tracks.
      $fasta_uri = $genome_files[0]['fastas'][0];
      $gff3_uri = $genome_files[0]['gff3s'][0];
      // Make sure we got a FASTA and a GFF3.
      if (!empty($fasta_uri) && !empty($gff3_uri)) {
        $management_form['jbrowse'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('JBrowse'),
          'genome' => [
            '#type' => 'hidden',
            '#value' => $data_model->id(),
          ],
          'fasta' => [
            '#type' => 'hidden',
            '#value' => $fasta_uri,
          ],
          'gff3' => [
            '#type' => 'hidden',
            '#value' => $gff3_uri,
          ],
          'load' => [
            '#type' => 'submit',
            '#value' => $this->t('Load genome in JBrowse'),
            '#submit' => [[$this, 'submitJbrowseLoading']],
          ],
        ];
        $event->setManagementForm($management_form);
      }
    }
  }

  /**
   * Load data into JBrowse.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitJbrowseLoading(array &$form, FormStateInterface $form_state) {
    $dataset = $form_state->getFormObject()->getEntity();
    $genome = $form_state->getValue(['data_models', 'genome', 'jbrowse', 'genome']);
    $fasta_file_uri = $form_state->getValue(['data_models', 'genome', 'jbrowse', 'fasta']);
    $gff3_file_uri = $form_state->getValue(['data_models', 'genome', 'jbrowse', 'gff3']);
    $fasta_file_name = substr(strrchr($fasta_file_uri, '/'), 1);
    $gff3_file_name = substr(strrchr($gff3_file_uri, '/'), 1);
    if (!empty($fasta_file_name) && !empty($gff3_file_name)) {
      // Debug.
      \Drupal::messenger()->addMessage('JBrowse loading: Not implemented');
      // @todo Check execution.
      $http_client = \Drupal::service('http_client');
      try {
        $jbrowse_cgi = "http://genoring-jbrowse/jbrowse-cgi/load_track.cgi";
        $req_parameters = [
          'query' => [
            'fasta' => $fasta_file_name,
            'gff3' => $gff3_file_name,
            'genome' => $dataset->get('label'),
          ],
        ];
        $response = $http_client->request(
          'GET',
          $jbrowse_cgi,
          $req_parameters
        );
        $output = $response->getBody() . '';
        \Drupal::logger('genoring_jbrowse')->debug('Command output: {placeholder}', ['placeholder' => $output]); //+debug

        // Add corresponding link to menu.
        $menuitem = \Drupal::entityTypeManager()
          ->getStorage('menu_link_content')
          ->loadByProperties(['link.uri' => "internal:/jbrowse/index.html?data=data&tracks=$genome"]);
        if (empty($menuitem)) {
          $menuitem = [
            'title' => "JBrowse $genome",
            'link' => [
              'uri' => "internal:/jbrowse/index.html?data=data&tracks=$genome",
            ],
            'menu_name' => strtolower("jbrowse_$genome"),
            'expanded' => TRUE,
          ];
          MenuLinkContent::create($menuitem)->save();
        }
      }
      catch (GuzzleException $exception) {
        \Drupal::logger('genoring_jbrowse')->error('Error loading tracks. Message: ' . $exception->getMessage());
      }

    }
    else {
      // Debug.
      \Drupal::messenger()->addWarning('JBrowse loading: Missing data files.');
    }
  }

}
