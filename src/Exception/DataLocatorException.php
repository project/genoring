<?php

namespace Drupal\genoring\Exception;

/**
 * Exception thrown by data manager and data locator plugins.
 */
class DataLocatorException extends \RuntimeException {}
