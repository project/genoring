<?php

namespace Drupal\genoring\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class DatasetAddForm.
 *
 * Provides the add form for dataset entities.
 *
 * @ingroup genoring
 */
class DatasetAddForm extends DatasetFormBase {

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Create Dataset');
    return $actions;
  }

}
