<?php

namespace Drupal\genoring\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\genoring\Entity\Dataset;
use Drupal\genoring\Event\DataModelManagementEvent;
use Drupal\genoring\Event\DataModelsEvent;
use Drupal\genoring\Event\GenoringEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DatasetFormBase.
 *
 * Base form used for creation, edit and delete forms for GenoRing datasets.
 *
 * @ingroup genoring
 */
class DatasetFormBase extends EntityForm {

  /**
   * An entity query factory for the dataset entity type.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * Construct the DatasetFormBase.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   An entity query factory for the dataset entity type.
   */
  public function __construct(EntityStorageInterface $entity_storage) {
    $this->entityStorage = $entity_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = new static($container->get('entity_type.manager')->getStorage('genoring_dataset'));
    $form->setMessenger($container->get('messenger'));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $dataset = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      // @todo Add a mecanism to change this label according to a selected model name.
      '#title' => $this->t('Dataset name'),
      '#maxlength' => 255,
      '#default_value' => $dataset->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine name'),
      '#default_value' => $dataset->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'replace_pattern' => '([^a-z0-9_]+)|(^custom$)',
        'error' => 'The machine-readable name must be unique, and can only contain lowercase letters, numbers, and underscores. Additionally, it can not be the reserved word "custom".',
      ],
      '#disabled' => !$dataset->isNew(),
    ];

    // List data files and provide file management interface.
    $form['data_files'] = $this->getFileManagementForm([], $form_state);
    $form['data_models'] = $this->getDataModelManagementForm([], $form_state);

    return $form;
  }

  /**
   * Provides the sub-form to manage dataset files.
   *
   * @param array $form
   *   A default form that could be empty.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The dataset file management form.
   */
  public function getFileManagementForm(
    array $form,
    FormStateInterface $form_state,
  ) :array {
    $dataset = $this->entity;
    $files = $dataset->getDataFiles();
    if (empty($files)) {
      $form += [
        '#type' => 'container',
      ];
      return $form;
    }

    $form += [
      '#type' => 'details',
      '#title' => $this->t('Data files'),
      '#tree' => TRUE,
    ];

    // List files.
    foreach ($dataset->getDataFiles() as $file_index => $data_file) {
      $form[$file_index] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['container-inline']],
      ];
      $form[$file_index]['data_file'] = [
        '#type' => 'item',
        '#markup' => $this->t(
          'File: @filename',
          ['@filename' => $data_file->getFilename()],
        ),
      ];
      $form[$file_index]['replace_file'] = [
        '#type' => 'submit',
        '#name' => 'replace_file',
        '#limit_validation_errors' => [],
        '#submit' => [[$this, 'submitReplaceFile']],
        '#value' => $this->t('Replace'),
      ];
      $form[$file_index]['remove_file'] = [
        '#type' => 'submit',
        '#name' => 'remove_file',
        '#limit_validation_errors' => [],
        '#submit' => [[$this, 'submitRemoveFile']],
        '#value' => $this->t('Remove'),
      ];
      // @todo Add file metadata management.
      // @todo Add file operations.
    }
    // Add new file.
    $form['add_file'] = [
      '#type' => 'genoring_input',
      '#title' => $this->t('New data file'),
    ];
    $form['add_file'] = [
      '#type' => 'submit',
      '#name' => 'add_file',
      '#limit_validation_errors' => [],
      '#submit' => [[$this, 'submitAddFile']],
      '#value' => $this->t('Add'),
    ];
    return $form;
  }

  /**
   * Provides the sub-form to manage dataset data models.
   *
   * @param array $form
   *   A default form that could be empty.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The dataset data model management form.
   */
  public function getDataModelManagementForm(
    array $form,
    FormStateInterface $form_state,
  ) :array {
    $dataset = $this->entity;
    $data_models = $dataset->getDataModelInstances();

    if (empty($data_models)) {
      $form += [
        '#type' => 'container',
      ];
    }
    else {
      $form += [
        '#type' => 'fieldset',
        '#title' => $this->t('Data models'),
        '#tree' => TRUE,
      ];
      // @todo Add dataset settings such as privacy status, etc.
      // Add data model management.
      // - List of managed data models (with used data files)
      //   Trigger data model management event with used model and data files to
      //   get a management form (action buttons with own validate/submit
      //   actions, reports, last updates, etc.) and a "remove data model"
      //   button.
      foreach ($data_models as $model_id => $model) {
        $form[$model_id] = [];
        $event = new DataModelManagementEvent($model);
        \Drupal::service('event_dispatcher')->dispatch(
          $event,
          GenoringEvents::DATA_MODEL_MANAGEMENT
        );
        $management_form = $event->getManagementForm();
        if (!empty($management_form)) {
          $form[$model_id] += $management_form;
        }
        else {
          $form[$model_id] += [
            'info' => [
              '#type' => 'item',
              '#markup' => $this->t('INFO: This data model has currently no operations that could be applied.'),
            ],
          ];
        }
        // @todo To complete.
      }
    }

    // Add data model.
    // Get list of available data models.
    $event = new DataModelsEvent();
    \Drupal::service('event_dispatcher')->dispatch(
      $event,
      GenoringEvents::DATA_MODELS
    );
    $data_model_definitions = $event->getDataModelDefinitions();
    $data_models = [
      '' => '-- Select --',
    ];
    foreach ($data_model_definitions as $data_model => $data_model_definition) {
      $data_models[$data_model] = $data_model_definition['label'];
    }
    $current_data_model = $form_state->getValue('data_model', '');
    $data_model_details_id = 'data-model-details-wrapper';
    $form['add_data_model'] = [
      '#type' => 'container',
    ];
    $form['add_data_model']['data_model'] = [
      '#type' => 'select',
      '#title' => $this->t('Data Model'),
      '#default_value' => $current_data_model,
      '#required' => TRUE,
      '#options' => $data_models,
      '#ajax' => [
        'callback' => [$this, 'buildAjaxDataModelForm'],
        'wrapper' => $data_model_details_id,
        'method' => 'replaceWith',
        'effect' => 'fade',
      ],
    ];

    if (empty($data_model_definitions[$current_data_model])) {
      $form['add_data_model']['data_model_details'] = [
        '#type' => 'container',
        '#attributes' => [
          'id' => $data_model_details_id,
        ],
      ];
    }
    else {
      $data_model = $data_model_definitions[$current_data_model];
      $form['add_data_model']['data_model_details'] = [
        '#type' => 'details',
        '#title' => $data_model['label'],
        '#open' => TRUE,
        'description' => [
          '#type' => 'item',
          '#markup' => $data_model['description'],
        ],
        '#attributes' => [
          'id' => $data_model_details_id,
        ],
      ];
      foreach ($data_model['fields'] as $field_name => $field_definition) {
        $form['add_data_model']['data_model_details'][$field_name] = genoring_generate_data_form_field($field_name, $field_definition);
      }
    }
    return $form;
  }

  /**
   * Handles switching the selected field mapper plugin.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public static function buildAjaxDataModelForm(array $form, FormStateInterface $form_state) {
    return $form['data_models']['add_data_model']['data_model_details'] ?? [];
  }

  /**
   * Checks for an existing dataset.
   *
   * @param string|int $entity_id
   *   The entity ID.
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if this format already exists, FALSE otherwise.
   */
  public function exists($entity_id, array $element, FormStateInterface $form_state) {
    $query = $this->entityStorage->getQuery();
    $result = $query->condition('id', $element['#field_prefix'] . $entity_id)
      ->accessCheck()
      ->execute();
    return (bool) $result;
  }

  /**
   * Get available data files list.
   *
   * Get the list of files from GenoRing dataset directory not already in use
   * by the given dataset.
   *
   * @param \Drupal\genoring\Entity\Dataset|null $dataset
   *   The current dataset. If not provided, $this->entity will be used.
   *
   * @return array
   *   An array of data files that could be used by this dataset. Keys are file
   *   URIs and values are file names.
   */
  public function getAvailableDataFiles(?Dataset $dataset) :array {
    // @todo Function to remove. To be transfered in DataManager.
    $file_options = [];
    $dataset ??= $this->entity;
    // @todo Turn this into a service.
    $file_registry_uri = \Drupal::state()->get('genoring_data_registry_uri', 'private://genoring/genoring_registry.tsv');
    if (file_exists($file_registry_uri)) {
      $registry_data = file_get_contents($file_registry_uri);
      // Remove comments.
      $registry_data = preg_replace('/^\s*#[^\n]*(?:\n|$)/m', '', $registry_data);
      // Parse file metadata.
      $files_metadata = array_map(
        function ($file_metadata) {
          return explode("\t", $file_metadata);
        },
        array_filter(explode("\n", $registry_data))
      );
      $used_files = $dataset->getDataFileUris();
      foreach ($files_metadata as $file_metadata) {
        if (!in_array($file_metadata[2], $used_files)) {
          $file_options[$file_metadata[2]] = $file_metadata[1];
        }
      }
    }
    return $file_options;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (($trigger = $form_state->getTriggeringElement())
        && in_array($trigger['#name'] ?? '', ['replace_file', 'remove_file'])
    ) {
      // No validation.
      return;
    }
    parent::validateForm($form, $form_state);
    // @todo Check for file change/removal if the file is in use by a data
    // model.
    // @todo Make sure new_data_file is in the dataset registry.
    $new_file = $form_state->getValue(['data_files', 'new_data_file'], '');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $dataset = $this->getEntity();
    $file_uris = $dataset->getDataFileUris();
    $file_usage = \Drupal::service('file.usage');

    // Check for data file actions.
    if ($trigger = $form_state->getTriggeringElement()) {
      if ('replace_file' == $trigger['#name']) {
        $file_index = $trigger['#parents'][1];
        // @todo Manage file replacement.
        $this->messenger()->addWarning('Not implemented.');
      }
      elseif ('remove_file' == $trigger['#name']) {
        $file_index = $trigger['#parents'][1];
        $removed_file = $file_uris[$file_index];
        // Keep only file name.
        if ($slash_pos = strrpos($removed_file, '/')) {
          $removed_file = substr($removed_file, $slash_pos + 1);
        }
        unset($file_uris[$file_index]);
        $dataset
          ->setDataFileUris(array_values($file_uris))
          ->save();
        // $form_state->setValue('fileUris', array_values($file_uris));
        $this->messenger()->addMessage('File ' . $removed_file . ' has been removed from the dataset successfully.');
        // @todo The file is removed from the dataset but is still managed by
        // Drupal. That file may or may not be in use by another dataset. We
        // should provide a separate interface to manage data files.
        $form_state->setRebuild(TRUE);
        return;
      }
      elseif (preg_match('/add_model_(\w+)/', $trigger['#name'], $matches)) {
        $model_id = $matches[1];
        $entity_type_manager = \Drupal::entityTypeManager();
        $entity_display_repository = \Drupal::service('entity_display.repository');
        // @todo Create a new external entity data model.
        $model_definition = $form_state->get('models.' . $model_id);
        $xntt_type_id = $dataset->id() . '__' . $model_id;
        // Check if the external entiy type already exists.
        $xntt = $entity_type_manager->getStorage('external_entity_type')->load($xntt_type_id);
        if (empty($xntt)) {
          $data_aggregator_config = [
            // @todo 'vertical' will be replaced by data model aggregator when
            // released.
            'id' => 'vertical',
            'config' => [
              'storage_clients' => $model_definition['storage_clients'],
            ],
          ];
          // Create a new external entity type as data model.
          $xntt = $entity_type_manager->getStorage('external_entity_type')->create([
            'id' => $xntt_type_id,
            'label' => $dataset->label() . ' ' . $model_definition['title'],
            'label_plural' => $dataset->label() . ' ' . $model_definition['title'],
            'description' => '',
            'generate_aliases' => FALSE,
            'read_only' => FALSE,
            'debug_level' => 0,
            'field_mappers' => [],
            'data_aggregator' => $data_aggregator_config,
            'persistent_cache_max_age' => 0,
          ]);
          $xntt->save();
        }
        else {
          $this->messenger()->addWarning(
            $this->t(
              'WARNING: the model @model already exists.',
              [
                '@model' => $xntt_type_id,
              ]
            )
          );
        }
        // Manage fields.
        foreach ($model_definition['fields'] as $field_name => $field_settings) {
          try {
            // Storage.
            $field_storage_config = $entity_type_manager
              ->getStorage('field_storage_config')
              ->load($xntt_type_id . '.' . $field_name);
            if (empty($field_storage_config)) {
              $field_storage_config = $entity_type_manager
                ->getStorage('field_storage_config')
                ->create([
                  'field_name' => $field_name,
                  'entity_type' => $xntt_type_id,
                ]
                + $field_settings['storage']);
              $field_storage_config->save();
            }
            // Field config.
            $field_config_id =
              $xntt_type_id
              . '.'
              . $xntt_type_id
              . '.'
              . $field_name;
            $field_config = $entity_type_manager
              ->getStorage('field_config')
              ->load($field_config_id);
            if (empty($field_config)) {
              $field_config_data = [
                'field_name' => $field_name,
                'entity_type' => $xntt_type_id,
                'bundle' => $xntt_type_id,
              ]
              + $field_settings['config']
              + [
                'label' => ucfirst($field_name),
                'description' => '',
              ];

              $entity_type_manager
                ->getStorage('field_config')
                ->create($field_config_data)
                ->save();
            }
            // Entity form display.
            if (!empty($field_settings['form_display'])) {
              $entity_display_repository
                ->getFormDisplay($xntt_type_id, $xntt_type_id, 'default')
                ->setComponent($field_name, $field_settings['form_display'])
                ->save();
            }
            // Entity view display.
            if (!empty($field_settings['view_display'])) {
              $entity_display_repository
                ->getViewDisplay($xntt_type_id, $xntt_type_id)
                ->setComponent($field_name, $field_settings['view_display'])
                ->save();
            }
          }
          catch (\Exception $e) {
            $error = TRUE;
            $this->messenger()->addError(
              $this->t(
                'There was a problem managing field %label: @message',
                ['%label' => $field_name, '@message' => $e->getMessage()]
              )
            );
          }
        }
        // Since we added new fields, we need to clear new external entity field
        // cache to define their mapping.
        $xntt->getMappableFields(TRUE);
        // Mapping definition.
        foreach ($model_definition['mapping'] as $field_name => $field_mapping) {
          $xntt
            ->setFieldMapperId($field_name, $field_mapping['id'])
            ->setFieldMapperConfig($field_name, $field_mapping['config']);
        }
        // Save new mapping.
        $xntt->save();

        // Lock xntt elements.
        $xntt_locked = \Drupal::state()->get('external_entities.type.locked');
        $xntt_locked[$xntt_type_id] = [
          'lock_edit' => FALSE,
          'lock_delete' => FALSE,
          'lock_fields' => FALSE,
          'lock_display' => FALSE,
          'lock_annotations' => FALSE,
          'lock_field_mappers' => [
            '*' => [
              'allow_plugins' => [],
              'lock_config' => TRUE,
              'hide_config' => FALSE,
            ],
          ],
          'lock_data_aggregator' => [
            'allow_plugins' => [],
            'lock_config' => TRUE,
            'hide_config' => FALSE,
            'lock_storage_clients' => [
              '*' => [
                'allow_plugins' => [],
                'lock_config' => TRUE,
                'hide_config' => FALSE,
              ],
            ],
          ],
        ];
        // Save lock changes for all examples.
        \Drupal::state()->set('external_entities.type.locked', $xntt_locked);

        // Add model to dataset.
        $data_models = $dataset->getDataModels();
        $data_models[] = substr($xntt->id(), (strpos($xntt->id(), '__') ?: -2) + 2);
        $data_models = array_unique($data_models);
        $dataset->setDataModels($data_models);
        $dataset->save();
        $this->messenger()->addMessage('Data model ' . $model_id . ' for dataset ' . $dataset->id() . ' has been created.');
      }
    }

    // Check for new data file.
    $new_file = $form_state->getValue(['data_files', 'new_data_file'], '');
    if (!empty($new_file)) {
      $file_uris[] = $new_file;
      $form_state->setValue('fileUris', $file_uris);
      // @todo Mark file as used.
      // @code
      // $file = ...
      // $file_usage->
      //   add($file, 'genoring', 'genoring_dataset', $dataset->id());
      // @endcode
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitReplaceFile(array &$form, FormStateInterface $form_state) {
    if (($trigger = $form_state->getTriggeringElement())
        && ($file_index = $trigger['#parents'][1] ?? '')
    ) {
      $dataset = $this->getEntity();
      $file_uris = $dataset->getDataFileUris();
      $replaced_file = $file_uris[$file_index];
      // @todo Manage file replacement.
      $this->messenger()->addWarning('Not implemented.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitRemoveFile(array &$form, FormStateInterface $form_state) {
    if (($trigger = $form_state->getTriggeringElement())
        && ($file_index = $trigger['#parents'][1] ?? '')
    ) {
      $dataset = $this->getEntity();
      $file_uris = $dataset->getDataFileUris();
      $removed_file = $file_uris[$file_index];

      // Keep only file name.
      if ($slash_pos = strrpos($removed_file, '/')) {
        $removed_file = substr($removed_file, $slash_pos + 1);
      }
      unset($file_uris[$file_index]);
      $dataset
        ->setDataFileUris(array_values($file_uris))
        ->save();
      $this->messenger()->addMessage('File ' . $removed_file . ' has been removed from the dataset successfully.');
      // @todo The file is removed from the dataset but is still managed by
      // Drupal. That file may or may not be in use by another dataset. We
      // should provide a separate interface to manage data files.
      $form_state->setRebuild(TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitAddFile(array $form, FormStateInterface $form_state) {
    // @todo Implement.
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $dataset = $this->getEntity();

    $status = $dataset->save();

    $url = $dataset->toUrl();

    $edit_link = Link::fromTextAndUrl($this->t('Edit'), $url)->toString();

    if ($status == SAVED_UPDATED) {
      $this->messenger()->addMessage(
        $this->t('Dataset %label has been updated.',
        ['%label' => $dataset->label()])
      );
      $this->logger('genoring')->notice(
        'Dataset %label has been updated.',
        ['%label' => $dataset->label(), 'link' => $edit_link]
      );
    }
    else {
      $this->messenger()->addMessage(
        $this->t('Dataset %label has been added.',
        ['%label' => $dataset->label()])
      );
      $this->logger('genoring')->notice(
        'Dataset %label has been added.',
        ['%label' => $dataset->label(), 'link' => $edit_link]
      );
    }

    // $form_state->setRedirect('entity.genoring_dataset.list');
    return $status;
  }

}
