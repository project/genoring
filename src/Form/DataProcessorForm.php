<?php

namespace Drupal\genoring\Form;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\File\FileExists;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\genoring\DataProcessor\DataProcessorInterface;
use Drupal\genoring\Event\GenoringEvents;
use Drupal\genoring\Event\FileTypesEvent;
use Drupal\genoring\Event\TypeMetadataEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Provides the data processor form.
 *
 * The data processor form task is to process an uploaded file and move it to
 * a directory managed by GenoRing. The file can be either already here in the
 * upload directory, or can be uploaded through the form. Then, the file is
 * processed according to its type and the selected data processors using the
 * Drupal batch API and, if needed, the Tripal task API.
 * A "<data_file_name>.metadata.yml" will be created with the provided and
 * gathered file metadata.
 * The final file and its related files will be automatically placed in a given
 * directory in the GenoRing data directory.
 * Finally, the file will be added to the data file index.
 */
class DataProcessorForm extends FormBase {

  /**
   * The data processor manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $dataProcessorManager;

  /**
   * The current data processing context.
   *
   * @var array
   */
  protected $context = [];

  /**
   * Constructs an DataProcessorForm object.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $data_processor_manager
   *   The data processor manager.
   */
  public function __construct(
    PluginManagerInterface $data_processor_manager,
  ) {
    $this->dataProcessorManager = $data_processor_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.genoring.data_processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'genoring_data_processor_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($form_state->has('step') && $form_state->get('step') == 2) {
      return $this->buildDataPreprocessingForm($form, $form_state);
    }

    return $this->buildUploadSelectionForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function buildUploadSelectionForm(array $form, FormStateInterface $form_state) {
    $form_state->set('step', 1);

    // Get supported file types.
    $event = new FileTypesEvent();
    \Drupal::service('event_dispatcher')->dispatch(
      $event,
      GenoringEvents::FILE_TYPES
    );
    $supported_file_types = $event->getFileTypes();
    $extensions = implode(' ', $event->getFileExtensions());
    $form_state->set('supported_file_types', $supported_file_types);

    $form['info'] = [
      '#type' => 'item',
      '#title' => $this->t(
        'You can either upload or select an already uploaded file and then provide its metadata to be able to use it in datasets.'
      ),
    ];

    // Get the list of uploaded files.
    $local_file_options = [];
    // @todo Allow other modules to specify other directories to look in,
    // through event + event handlers.
    $local_file_options = $this->listFiles('private://upload/');

    // Select between an already uploaded file and upload form element.
    $form['location_selection'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('File location'),
      'file_location' => [
        '#type' => 'radios',
        '#title' => $this->t('Source selection'),
        '#options' => [
          'upload' => $this->t('Upload a new file'),
          'local' => $this->t('Select an already uploaded file'),
        ],
        '#required' => TRUE,
        '#default_value' => $form_state->getValue('file_location', 'upload'),
      ],
      // We need to wrap the managed_file field into a container to get arround
      // issue https://www.drupal.org/project/drupal/issues/2847425.
      'new_data_file_container' => [
        '#type' => 'container',
        '#states' => [
          'visible' => [
            ':input[name="file_location"]' => ['value' => 'upload'],
          ],
        ],
        'new_data_file' => [
          '#type' => 'managed_file',
          '#title' => $this->t('Upload new data file'),
          '#upload_location' => \Drupal::state()->get('genoring_upload_uri', 'private://upload'),
          '#upload_validators' => [
            'FileExtension' => [
              'extensions' => $extensions,
            ],
          ],
        ],
      ],
      'uploaded_data_file' => [
        '#type' => 'select',
        '#title' => $this->t('Select an already uploaded file'),
        '#options' => $local_file_options,
        '#empty_option' => $this->t('- None -'),
        '#empty_value' => '',
        '#default_value' => $form_state->getValue('uploaded_data_file', ''),
        '#states' => [
          'visible' => [
            ':input[name="file_location"]' => ['value' => 'local'],
          ],
        ],
      ],
    ];

    // Provide metadata form.
    $form['meta'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Metadata'),

      // Select file type.
      'file_type' => [
        '#type' => 'select',
        '#title' => $this->t('File type'),
        '#default_value' => $form_state->getValue('file_type', ''),
        '#options' => $this->getFileTypes($form_state),
        '#required' => TRUE,
      ],

      // File description.
      'description' => [
        '#type' => 'textarea',
        '#title' => $this->t('File description'),
        '#description' => $this->t('Enter the description.'),
        '#default_value' => $form_state->getValue('description', ''),
        '#required' => TRUE,
      ],

      // File version.
      'version' => [
        '#type' => 'textfield',
        '#title' => $this->t('Version'),
        '#default_value' => $form_state->getValue('version', ''),
        '#required' => FALSE,
      ],

      // File date.
      'date' => [
        '#type' => 'date',
        '#title' => $this->t('File generation date'),
        '#description' => $this->t('Date when the file was generated or created. If not know, an approximate date will be ok (replace unknown day or month by "01").'),
        '#default_value' => $form_state->getValue('date', date('Y-m-d')),
        '#required' => TRUE,
      ],

      // File origin/provider.
      'provider' => [
        '#type' => 'textfield',
        '#title' => $this->t('File origin/provider'),
        '#description' => $this->t('Name of the person, company, and/or technology that generated the file.'),
        '#default_value' => $form_state->getValue('provider', ''),
        '#required' => FALSE,
      ],

      // License.
      'license' => [
        '#type' => 'select',
        '#title' => $this->t('License'),
        '#options' => $this->getLicenses($form_state),
        '#default_value' => $form_state->getValue('license', 'Unknown'),
        '#required' => TRUE,
      ],
      'license_other' => [
        '#type' => 'textfield',
        '#title' => $this->t('Other license:'),
        '#default_value' => $form_state->getValue('license_other', ''),
        '#required' => FALSE,
        '#states' => [
          'visible' => [
            ':input[name="license"]' => ['value' => 'Other'],
          ],
        ],
      ],

      // Restrictions.
      'restrictions' => [
        '#type' => 'select',
        '#title' => $this->t('Restrictions'),
        '#options' => [
          'private' => $this->t('Private'),
          'partners' => $this->t('Open to allowed collaborators'),
          'await' => $this->t('Awaiting publication (temp. restricted)'),
        ],
        '#empty_option' => $this->t('Public'),
        '#empty_value' => '',
        '#default_value' => $form_state->getValue('restrictions', ''),
        '#required' => FALSE,
      ],

      // Notes.
      'notes' => [
        '#type' => 'textarea',
        '#title' => $this->t('Notes'),
        '#description' => $this->t('Additional notes can be provided here if needed.'),
        '#default_value' => $form_state->getValue('notes', ''),
        '#required' => FALSE,
      ],

    ];

    // Actions.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['next'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Next'),
      '#validate' => ['::validateNextUploadSelectionForm'],
      '#submit' => ['::submitNextUploadSelectionForm'],
    ];

    return $form;
  }

  /**
   * List available uploaded files.
   *
   * @param string $directory
   *   Path of directory to process.
   * @param bool $include_subdir
   *   If TRUE, include files in sub-directories.
   *
   * @return array
   *   An array of form options for a 'select' form element.
   */
  protected function listFiles(string $directory, bool $include_subdir = FALSE) :array {
    $file_options = [];
    if ($path = \Drupal::service('file_system')->realpath($directory)) {
      $iterator = new \DirectoryIterator($path);
      foreach ($iterator as $file_info) {
        if ($file_info->isFile()) {
          $file_options[$directory . $file_info->getFilename()] = $file_info->getFilename();
        }
        elseif ($include_subdir && $file_info->isDir()) {
          // @todo Support sub-directories.
          // $file_options += $this->listFiles($file_info->getPath());
        }
      }
    }
    return $file_options;
  }

  /**
   * List supported file types.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   An array of file type options for a 'select' form element.
   */
  protected function getFileTypes(FormStateInterface $form_state) :array {
    $supported_file_types = $form_state->get('supported_file_types');
    $file_types = [];
    foreach ($supported_file_types as $file_type => $type_definition) {
      $categories = $type_definition['categories'];
      if (!empty($categories)) {
        foreach ($categories as $category) {
          // Translate categories here and cast them to string because the
          // "select" form element needs options to be strings in order to work
          // properly, otherwise, objects are treated differently.
          $category = implode('/', array_map(
            function ($cat) {
              // The cat(egory) string comes from a literal string.
              // phpcs:ignore
              return (string) $this->t($cat);
            },
            $category
          ));
          NestedArray::setValue(
            $file_types,
            [$category, $file_type],
            // The type definition name comes from a literal string.
            // phpcs:ignore
            (string) $this->t($type_definition['name'])
          );
        }
      }
      else {
        $file_types[$file_type] = $type_definition['name'];
      }
    }

    return $file_types;
  }

  /**
   * List licenses.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   An array of available license options for a 'select' form element.
   */
  protected function getLicenses(FormStateInterface $form_state) :array {
    return [
      'Unknown' => 'Unknown',
      'CC0' => 'CC0 (No Rights Reserved, public domain)',
      'CC-by' => 'CC-by (Attribution)',
      'CC-by-sa' => 'CC-by-sa (Attribution-ShareAlike)',
      'CC-by-nc' => 'CC-by-nc (Attribution-NonCommercial)',
      'CC-by-nc-sa' => 'CC-by-nc-sa (Attribution-NonCommercial-ShareAlike)',
      'CC-by-nd' => 'CC-by-nd (Attribution-NoDerivatives)',
      'CC-by-nc-nd' => 'CC-by-nc-nd (Attribution-NonCommercial-NoDerivatives)',
      'GNU AGPLv3' => 'GNU Affero General Public License v3.0',
      'GNU GPLv3' => 'GNU General Public License v3.0',
      'GNU LGPLv3' => 'GNU Lesser General Public License v3.0',
      'Mozilla Public License 2.0' => 'Mozilla Public License 2.0',
      'Apache License 2.0' => 'Apache License 2.0',
      'MIT License' => 'MIT License',
      'The Unlicense' => 'The Unlicense',
      'Other' => 'Other',
    ];
  }

  /**
   * Provides custom validation handler for page 1.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateNextUploadSelectionForm(array &$form, FormStateInterface $form_state) {
    // Save uploaded file if one.
    if ($file_location = $form_state->getValue('file_location')) {
      if ('upload' == $file_location) {
        $new_file = $form_state->getValue(['new_data_file'], []);
        if (isset($new_file[0]) && !empty($new_file[0])) {
          $file = File::load($new_file[0]);
          $file->setPermanent();
          $file->save();
          $form_state->set('file_uri', $file->getFileUri());
        }
      }
      elseif ('local' == $file_location) {
        $form_state->set('file_uri', $form_state->getValue('uploaded_data_file'));
      }
    }
    // Make sure we got a file name to work on.
    if (!$form_state->has('file_uri') || empty($form_state->get('file_uri'))) {
      $form_state->setErrorByName('file_location', $this->t('Please select a file source and provide a valid file.'));
    }
  }

  /**
   * Provides custom submission handler for page 1.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitNextUploadSelectionForm(array &$form, FormStateInterface $form_state) {
    // Save current values in case we go back.
    $form_state->set('upload_selection_values', $form_state->getValues());

    $license = $form_state->getValue('license', 'Unknown');
    if ('Other' == $license) {
      $license = $form_state->getValue('license_other', 'Unknown');
    }
    $form_state
      ->set(
        'metadata',
        [
          'file_type' => $form_state->getValue('file_type', ''),
          'description' => $form_state->getValue('description', ''),
          'version' => $form_state->getValue('version', ''),
          'date' => $form_state->getValue('date', ''),
          'provider' => $form_state->getValue('provider', ''),
          'license' => $license,
          'notes' => $form_state->getValue('notes', ''),
        ]
      )
      ->set('step', 2)
      ->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function buildDataPreprocessingForm(array $form, FormStateInterface $form_state) {
    $form_state->set('step', 2);

    // Get current metadata.
    $metadata = $form_state->get('metadata');
    // According to the given data type and metadata, get available data
    // processors.
    $data_processor_ids = $this->dataProcessorManager->getDataProcessorsForType($metadata['file_type']);
    $data_processors = [];
    foreach ($data_processor_ids as $data_processor_id) {
      $data_processors[$data_processor_id] = $this->dataProcessorManager->createInstance($data_processor_id, []);
    }

    // Display current metadata.
    $form['meta'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Metadata'),
      '#tree' => TRUE,
      'file_uri' => [
        '#type' => 'item',
        '#title' => $this->t('File'),
        '#markup' => $form_state->get('file_uri'),
      ],
      'file_type' => [
        '#type' => 'item',
        '#title' => $this->t('File type'),
        '#markup' => $metadata['file_type'],
      ],
    ];
    // Add additional metadata according to type.
    $event = new TypeMetadataEvent($metadata['file_type'], 'file');
    \Drupal::service('event_dispatcher')->dispatch(
      $event,
      GenoringEvents::DATA_TYPE_METADATA
    );
    $metadata_fields = $event->getMetadataFields();
    // @todo Make sure existing metadata fields are not replaced.
    $form['meta'] = array_merge($form['meta'], $metadata_fields);

    $form['data_processing'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Data processing'),
    ];

    // For each data processor, provide checkboxes to enabled/disable the data
    // processor.
    foreach ($data_processors as $data_processor_id => $data_processor) {
      $form['data_processing'][$data_processor_id] = [
        '#type' => 'container',
      ];
      $data_processor_form_state = GenoringSubformState::createForSubform(
        ['data_processing', $data_processor_id],
        $form,
        $form_state
      );
      $form['data_processing'][$data_processor_id] = $data_processor->buildConfigurationForm(
        $form['data_processing'][$data_processor_id],
        $data_processor_form_state
      );
    }

    // Provide a dropdown to archive or not original file in a given compressed
    // format.
    $form['data_processing']['archive'] = [
      '#type' => 'select',
      '#title' => $this->t('Archive original data'),
      '#description' => $this->t('By default, original data is not kept to save space. Selecting an archive format ensure the original data file will be compressed and kept. Depending on the file size and the available disk space, you may disable archiving here while you keep original files by yourself somewhere else.'),
      '#default_value' => $form_state->getValue(['data_processing', 'archive'], ''),
      '#options' => [
        // @todo Add a list of supported archive formats.
        '' => $this->t('No archive'),
      ],
    ];

    // File name management.
    $form['replace'] = [
      '#type' => 'select',
      '#title' => $this->t('If a file with the same name already exists'),
      '#default_value' => $form_state->getValue(['replace'], ''),
      '#options' => [
        '' => $this->t('Do nothing'),
        'replace' => $this->t('Replace existing file with the new one'),
        'rename' => $this->t('Automatically rename the new file'),
      ],
    ];

    // Actions.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['back'] = [
      '#type' => 'submit',
      '#value' => $this->t('Back'),
      '#submit' => ['::backToUploadSelectionForm'],
      '#limit_validation_errors' => [],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Perform data processors checks.
    $metadata = $form_state->get('metadata');
    // Check if target file already exists.
    $file_uri = $form_state->get('file_uri');
    $file_storage = \Drupal::entityTypeManager()->getStorage('file');
    $file_query = $file_storage->getQuery();
    $file_ids = $file_query->accessCheck(TRUE)->condition('uri', $file_uri)->execute();
    if (empty($file_ids)) {
      if (file_exists($file_uri)) {
        // Create a managed file.
        $file = File::create([
          'filename' => basename($file_uri),
          'uri' => $file_uri,
          'status' => File::STATUS_PERMANENT,
          'uid' => \Drupal::currentUser()->id(),
        ]);
        $file->save();
      }
      else {
        $form_state->setErrorByName('meta', $this->t('File @file_uri not found.', ['@file_uri' => $file_uri]));
        return;
      }
    }
    else {
      $file = $file_storage->load(current($file_ids));
    }
    $form_state->set('file', $file);
    $new_uri = \Drupal::state()->get('genoring_data_uri', 'private://genoring') . '/' . $file->getFilename();
    if (file_exists($new_uri) && empty($form_state->getValue('replace'))) {
      $form_state->setErrorByName('replace', $this->t('A file with the same name already exist. Check "replace" if you want to replace it. Otherwise, the uploaded file needs to be renamed.'));
    }
    // According to the given data type and metadata, get available data
    // processors.
    $this->context = [];
    $checks = [];
    $data_processor_ids = $this->dataProcessorManager->getDataProcessorsForType($metadata['file_type']);
    $data = file_get_contents($file->getFileUri());
    $form_state->set('file_data', $data);
    foreach ($data_processor_ids as $data_processor_id) {
      $data_processor = $this->dataProcessorManager->createInstance($data_processor_id, []);
      $data_processor_form_state = GenoringSubformState::createForSubform(
        ['data_processing', $data_processor_id],
        $form,
        $form_state
      );
      $data_processor->validateConfigurationForm(
        $form['data_processing'][$data_processor_id],
        $data_processor_form_state
      );
      $data_processor->submitConfigurationForm(
        $form['data_processing'][$data_processor_id],
        $data_processor_form_state
      );
      $checks = array_merge($checks, $data_processor->checkData($data, $this->context));
    }
    if (!empty($checks[DataProcessorInterface::STATUS_ERROR])) {
      $form_state->setErrorByName('file_uri', $this->t('File content error: @error', ['@error' => implode("<br/>\n", $checks[DataProcessorInterface::STATUS_ERROR])]));
    }
    if (!empty($checks[DataProcessorInterface::STATUS_WARNING])) {
      $this->messenger()->addWarning(implode("<br/>\n", $checks[DataProcessorInterface::STATUS_WARNING]));
    }
    if (!empty($checks[DataProcessorInterface::STATUS_NOTICE])) {
      $this->messenger()->addMessage(implode("<br/>\n", $checks[DataProcessorInterface::STATUS_NOTICE]));
    }
    if (!empty($checks[DataProcessorInterface::STATUS_INFO])) {
      $this->messenger()->addMessage(implode("<br/>\n", $checks[DataProcessorInterface::STATUS_INFO]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get metadata.
    $metadata = $form_state->get('metadata');
    $additional_metadata = $form_state->getValue('meta');

    // Perform data processors alterations.
    $data_processor_ids = $this->dataProcessorManager->getDataProcessorsForType($metadata['file_type']);
    $data_processors = [];
    foreach ($data_processor_ids as $data_processor_id) {
      $data_processors[$data_processor_id] = $this->dataProcessorManager->createInstance($data_processor_id, []);
    }

    // Move files to the GenoRing dataset directory.
    $file = $form_state->get('file');
    $file_repository = \Drupal::service('file.repository');
    $replace_mode = ('replace' == $form_state->getValue('replace', '')) ? FileExists::Replace : FileExists::Rename;
    $file = $file_repository->move($file, \Drupal::state()->get('genoring_data_uri', 'private://genoring') . '/' . $file->getFilename(), $replace_mode);
    // @todo Display the new file name $file->getFilename().
    // @todo Generate an archive if requested.
    $data = $form_state->get('file_data');
    foreach ($data_processors as $data_processor_id => $data_processor) {
      $data = $data_processor->processData($data, $this->context);
    }
    if (!empty($this->context['processings'])) {
      // Save new data.
      $file = $file_repository->writeData($data, $file->getFileUri(), FileExists::Replace);
    }

    // Generate metadata file.
    $metadata_uri = \Drupal::state()->get('genoring_data_uri', 'private://genoring') . '/' . $file->getFilename() . '.metadata.yml';
    $metadata_yaml = Yaml::dump(array_merge($metadata, $additional_metadata));
    $metafile = $file_repository->writeData($metadata_yaml, $metadata_uri, FileExists::Replace);
    $metafile->setPermanent();
    $metafile->save();

    // Update registry file.
    // @todo Turn this into a service.
    $file_registry_uri = \Drupal::state()->get('genoring_data_registry_uri', 'private://genoring/genoring_registry.tsv');
    if (file_exists($file_registry_uri)) {
      $registry_data = file_get_contents($file_registry_uri);
    }
    if (empty($registry_data)) {
      $registry_data = "#id\tfilename\turi\tmetadata\n";
    }
    elseif ("\n" !== $registry_data[-1]) {
      $registry_data .= "\n";
    }
    $registry_data .= $file->id() . "\t" . $file->getFilename() . "\t" . $file->getFileUri() . "\t" . $metadata_uri . "\n";
    $registry = $file_repository->writeData($registry_data, $file_registry_uri, FileExists::Replace);
    $registry->setPermanent();
    $registry->save();
    // Tell the user everything went well.
    $this->messenger()->addMessage('Data file added successfully.');
  }

  /**
   * Provides a handler to go back to the upload page.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function backToUploadSelectionForm(array &$form, FormStateInterface $form_state) {
    $form_state
      ->setValues($form_state->get('upload_selection_values'))
      ->set('step', 1)
      ->setRebuild(TRUE);
  }

}
