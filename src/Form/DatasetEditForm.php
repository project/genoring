<?php

namespace Drupal\genoring\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class DatasetEditForm.
 *
 * Provides the edit form for our Dataset entity.
 *
 * @ingroup genoring
 */
class DatasetEditForm extends DatasetFormBase {

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Update Dataset');
    return $actions;
  }

}
