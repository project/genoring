<?php

namespace Drupal\genoring\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Confirmation form used to reload taxonomy data.
 */
class ReloadTaxonomyForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'genoring_reload_taxonomy_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['remove_file'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Re-download speclits.txt file from UniProt'),
      '#default_value' => 0,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Reload all taxonomy data?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('genoring.dashboard');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This operation will remove any previous taxonomy data to completly reload the taxonomy.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Continue');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $taxonomy = \Drupal::service('genoring.taxonomy');
    if ($form_state->getValue('remove_file')) {
      $taxonomy->removeTaxonomyFile();
    }
    $taxonomy->loadTaxonomy();
    // Try to get Arabidopsis thaliana.
    $arath = $taxonomy->getSpecies(3702);
    if (!empty($arath)) {
      \Drupal::messenger()->addMessage('Taxonomy successfully reloaded.');
    }
    else {
      \Drupal::messenger()->addWarning('Failed to reload taxonomy. See logs for details.');
    }
    $form_state->setRedirectUrl(new Url('genoring.dashboard'));
  }

}
