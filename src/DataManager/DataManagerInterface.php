<?php

namespace Drupal\genoring\DataManager;

use Drupal\file\FileInterface;

/**
 * Interface for data manager.
 *
 * A data manager is used to locate data and manage uploaded files. It is also
 * used to manage data fields and metadata.
 *
 * Metadata is the data related to a given data element, usualy a file or a
 * directory (dataset). The metadata is structured as an associative array with
 * multiple levels. Keys are the metdata element names and values can be of any
 * type (single string, multiple strings, numeric values, a boolean,
 * sub-arrays, ...).
 * Some typical metadata keys are:
 * - identifier: An unambiguous reference to the resource within a given context.
 *     Recommended practice is to identify the resource by means of a string conforming to an identification system. Examples include International Standard Book Number (ISBN), Digital Object Identifier (DOI), and Uniform Resource Name (URN). Persistent identifiers should be provided as HTTP URIs.
 *     Dataset URL or file URI.
 * - title: A name given to the resource.
 * - type: The nature or genre of the resource.
 *     Recommended practice is to use a controlled vocabulary such as the DCMI Type Vocabulary [DCMI-TYPE]. To describe the file format, physical medium, or dimensions of the resource, use the property Format.
 *     Typical values are "file", "directory", or "url".
 * - format: The file format, physical medium, or dimensions of the resource.
 *     Recommended practice is to use a controlled vocabulary where available. For example, for file formats one could use the list of Internet Media Types [MIME]. Examples of dimensions include size and duration.
 *     Used for file format and MIME types as synonyms (ex. 'fasta', and 'text/x-fasta' can be used as a synonym). See https://www.iana.org/assignments/media-types/media-types.xhtml
 * - hasFormat: [] A related resource that is substantially the same as the pre-existing described resource, but in another format.
 *     This property is intended to be used with non-literal values. This property is an inverse property of Is Format Of.
 *     Filled by file converters on the source metadata.
 * - isFormatOf: A pre-existing related resource that is substantially the same as the described resource, but in another format.
 *     This property is intended to be used with non-literal values. This property is an inverse property of Has Format.
 *     Filled by file converters on the new generated file metadata.
 * - hasPart: [] A related resource that is included either physically or logically in the described resource.
 *     This property is intended to be used with non-literal values. This property is an inverse property of Is Part Of.
 *     Used by datasets to list included files.
 * - isPartOf: [] A related resource in which the described resource is physically or logically included.
 *     This property is intended to be used with non-literal values. This property is an inverse property of Has Part.
 *     Used by files that are part of datasets.
 * - coverage: [] The spatial or temporal topic of the resource, spatial applicability of the resource, or jurisdiction under which the resource is relevant.
 *     Spatial topic and spatial applicability may be a named place or a location specified by its geographic coordinates. Temporal topic may be a named period, date, or date range. A jurisdiction may be a named administrative entity or a geographic place to which the resource applies. Recommended practice is to use a controlled vocabulary such as the Getty Thesaurus of Geographic Names [TGN]. Where appropriate, named places or time periods may be used in preference to numeric identifiers such as sets of coordinates or date ranges. Because coverage is so broadly defined, it is preferable to use the more specific subproperties Temporal Coverage and Spatial Coverage.
 *     Used for file content (ex. "sequences", "alignment", descriptor number or name, etc.).
 * - created: Date of creation of the resource.
 *     Recommended practice is to describe the date, date/time, or period of time as recommended for the property Date, of which this is a subproperty.
 * - modified: Date on which the resource was changed.
 *     Recommended practice is to describe the date, date/time, or period of time as recommended for the property Date, of which this is a subproperty.
 * - conformsTo: [] An established standard to which the described resource conforms.
 *     Used to tag format specificities the data conforms to (ex. "sorted" for a GFF3).
 * - relation: [] A related resource.
 *     Recommended practice is to identify the related resource by means of a URI. If this is not possible or feasible, a string conforming to a formal identification system may be provided.
 *     Used to indicate derived files (ie. files generated using this file amongst others).
 * - source: [] A related resource from which the described resource is derived. URI.
 *     This property is intended to be used with non-literal values. The described resource may be derived from the related resource in whole or in part. Best practice is to identify the related resource by means of a URI or a string conforming to a formal identification system.
 *     Used for the original file name or to list files used to generate the current one.
 * - version: Version of current file or dataset.
 * - hasVersion: [] A related resource that is a version, edition, or adaptation of the described resource.
 *     Changes in version imply substantive changes in content rather than differences in format. This property is intended to be used with non-literal values. This property is an inverse property of Is Version Of.
 *     Used for other version URIs when a new version is released.
 * - isVersionOf: A related resource of which the described resource is a version, edition, or adaptation.
 *     Changes in version imply substantive changes in content rather than differences in format. This property is intended to be used with non-literal values. This property is an inverse property of Has Version.
 *     Used to link previous version when a new version is released.
 * - license: A legal document giving official permission to do something with the resource.
 *     Recommended practice is to identify the license document with a URI. If this is not possible or feasible, a literal value that identifies the license may be provided.
 * - rightsHolder: [] A person or organization owning or managing rights over the resource.
 *     Recommended practice is to refer to the rights holder with a URI. If this is not possible or feasible, a literal value that identifies the rights holder may be provided.
 *     Can be used for institutes.
 * - creator: [] An entity responsible for making the resource.
 *     Recommended practice is to identify the creator with a URI. If this is not possible or feasible, a literal value that identifies the creator may be provided.
 *     Used for authors.
 * - contributor: [] An entity responsible for making contributions to the resource.
 *     The guidelines for using names of persons or organizations as creators apply to contributors.
 *     Used for co-authors.
 * - description: An account of the resource.
 *     Description may include but is not limited to: an abstract, a table of contents, a graphical representation, or a free-text account of the resource.
 * - extent: The size or duration of the resource.
 *     Recommended practice is to specify the file size in megabytes and duration in ISO 8601 format.
 *     Used for the file size in Mb.
 * -  isReferencedBy: when used in datasets
 * - rights: Information about rights held in and over the resource.
 *     Typically, rights information includes a statement about various property rights associated with the resource, including intellectual property rights.
 * - accessRights: [] Information about who access the resource or an indication of its security status.
 *     Access Rights may include information regarding access or restrictions based on privacy, security, or other policies.
 *     Used to manage access restrictions.
 * - available: Date that the resource became or will become available.
 *     Recommended practice is to describe the date, date/time, or period of time as recommended for the property Date, of which this is a subproperty.
 * - tags: [] non-normalized terms with no values, attached to the data for searching and filtering.
 * - processor: The name of the main (or last) data processor (software, script, service) used to generate the file.
 * - processing: [] ordered list of treatments made to the source file to create this one.
 *     Each treatment is a command line used to generate the file with original paths replaced.
 * - taxonomy: [] List of taxonomy identifiers used by the file or dataset.
 *     Numeric codes are public taxonomy identifier used on ENA and NCBI.
 *     5-capital letter codes are UniProt species identification codes.
 *     Others are considered as custom taxonomy names.
 *     See https://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/docs/speclist.txt
 *
 * @see https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
 */
interface DataManagerInterface {

  /**
   * Mode used to completely replace an existing metadata array.
   */
  const METADATA_REPLACE = 0;

  /**
   * Mode used to only add missing metadata values, existing values are left.
   */
  const METADATA_COMPLETE = 1;

  /**
   * Mode used to add missing metadata values and override existing ones.
   */
  const METADATA_OVERRIDE = 2;

  /**
   * Returns the file matching the given metadata or NULL.
   *
   * @param array $metadata
   *   A metadata array.
   *
   * @return \Drupal\file\FileInterface|null
   *   A FileInterface instance if the file was found or NULL if the file was
   *   not found or if the metadata were not sufficient to locate the file.
   */
  public function getFile(array $metadata) :FileInterface|null;

  /**
   * Returns the file path that should correspond to the metadata.
   *
   * @param array $metadata
   *   A metadata array.
   * @param bool $provide_default
   *   If TRUE and no file path could be determined, it must return a default,
   *   non-empty path.
   *
   * @return string
   *   The file path. If the file path can not be determined and
   *   $provide_default is FALSE (default), an empty string is returned.
   */
  public function locateFilePath(
    array $metadata,
    bool $provide_default = FALSE,
  ) :string;

  /**
   * Returns the path to a directroy that should correspond to the metadata.
   *
   * @param array $metadata
   *   A metadata array.
   * @param bool $provide_default
   *   If TRUE and no file path could be determined, it must return a default,
   *   non-empty path.
   *
   * @return string
   *   The directory path. If the directory path can not be determined and
   *   $provide_default is FALSE (default), an empty string is returned.
   */
  public function locateDirectory(
    array $metadata,
    bool $provide_default = FALSE,
  ) :string;

  /**
   * Place an existing file on the file system according to the given metadata.
   *
   * @param \Drupal\file\FileInterface $file
   *   An existing file.
   * @param array $metadata
   *   A metadata array.
   *
   * @return \Drupal\file\FileInterface
   *   The new File object corresponding to the new file location.
   */
  public function placeFile(FileInterface $file, array $metadata) :FileInterface;

  /**
   * Place a set of files on the file system.
   *
   * @param array|string $file_list
   *   Either an array which keys are file URIs and values are metadata or a
   *   string containing a path to either a TSV or YAML file that contains a
   *   list of files with their metadata.
   *   The TSV file must have a first uncommented line with the column
   *   name corresponding to the metadata name and there must be at least a
   *   "uri" column and a "file_format" or "file_type" column.
   *   The YAML file should reflect the array structure: at first level, it must
   *   provide file URIs and as values an array of metadata for each file.
   *
   * @return \Drupal\file\FileInterface[]
   *   An array of File objects corresponding to the placed files.
   *
   * @throw \Drupal\genoring\Exception\DataLocatorException
   *   Thrown if the provided list is incorrect or does not contain the
   *   metadata required to place files.
   */
  public function placeFiles(array|string $file_list) :array;

  /**
   * Place a set of files of an archive on the file system.
   *
   * @param string $zip_uri
   *   URI of the archive file. Supported archive types depend on the plugin.
   * @param array|string $metadata
   *   Metadata string or array.
   * @param bool $extract_files
   *   If TRUE files will be extracted.
   *
   * @return \Drupal\file\FileInterface[]
   *   An array of File objects corresponding to the placed files.
   *
   * @throw \Drupal\genoring\Exception\DataLocatorException
   */
  public function placePackage(
    string $zip_uri,
    array|string $metadata,
    bool $extract_files = FALSE,
  ) :array;

  /**
   * Returns metadata associated to a file.
   *
   * @param string|FileInterface $file
   *   The file of interest, either its URI as a string or a FileInterface
   *   instance.
   *
   * @return array
   *   A metadata structure.
   */
  public function getMetadata(string|FileInterface $file) :array;

  /**
   * Adjust and save new metadata associated to a file.
   *
   * @param string|FileInterface $file
   *   The file of interest, either its URI as a string or a FileInterface
   *   instance.
   * @param array $metadata
   *   New metadata array.
   * @param int $mode
   *   Mode to manage provided metadata. One of:
   *   - METADATA_REPLACE: replaces the whole metadata array if one (default).
   *   - METADATA_COMPLETE: complete by only adding metadata values that are
   *     missing and leaving existing ones as thery are.
   *   - METADATA_OVERRIDE: replaces existing values, add missing ones and leave
   *     other existing values that are not provided in the new array.
   *
   * @return self
   *   Current data manager instance.
   */
  public function setMetadata(
    string|FileInterface $file,
    array $metadata,
    int $mode = DataManagerInterface::METADATA_REPLACE,
  ) :DataManagerInterface;

  /**
   * Generates a form field structure.
   *
   * @param string $field_name
   *   Field name/label.
   * @param array $field_definition
   *   GenoRing field definition.
   * @param array $override
   *   Form field override.
   *
   * @return array
   *   A form field structure.
   */
  public function generateFormField(
    string $field_name,
    array $field_definition,
    array $override,
  ) :array;

  /**
   * Generates a field config structure.
   *
   * @param string $field_name
   *   Field name/label.
   * @param array $field_definition
   *   GenoRing field definition.
   * @param array $override
   *   Form field override.
   *
   * @return array
   *   A field config structure.
   */
  public function generateFieldConfig(
    string $field_name,
    array $field_definition,
    array $override,
  ) :array;

  /**
   * Generates a field storage structure.
   *
   * @param string $field_name
   *   Field name/label.
   * @param array $field_definition
   *   GenoRing field definition.
   * @param array $override
   *   Form field override.
   *
   * @return array
   *   A field storage structure.
   */
  public function generateFieldStorage(
    string $field_name,
    array $field_definition,
    array $override,
  ) :array;

  /**
   * Generates a field view display structure.
   *
   * @param string $field_name
   *   Field name/label.
   * @param array $field_definition
   *   GenoRing field definition.
   * @param array $override
   *   Form field override.
   *
   * @return array
   *   A field view display structure.
   */
  public function generateFieldViewDisplay(
    string $field_name,
    array $field_definition,
    array $override,
  ) :array;

  /**
   * Generates a field form display structure.
   *
   * @param string $field_name
   *   Field name/label.
   * @param array $field_definition
   *   GenoRing field definition.
   * @param array $override
   *   Form field override.
   *
   * @return array
   *   A field form display structure.
   */
  public function generateFieldFormDisplay(
    string $field_name,
    array $field_definition,
    array $override,
  ) :array;

  /**
   * Generates an external entity field mapping structure.
   *
   * @param string $field_name
   *   Field name/label.
   * @param array $field_definition
   *   GenoRing field definition.
   * @param array $override
   *   Form field override.
   *
   * @return array
   *   An external entity field mapping structure.
   */
  public function generateFieldMapping(
    string $field_name,
    array $field_definition,
    array $override,
  ) :array;

}
