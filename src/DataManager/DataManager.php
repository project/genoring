<?php

namespace Drupal\genoring\DataManager;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Render\Element\Email;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\file\FileInterface;
use Drupal\genoring\Exception\DataLocatorException;
use Drupal\link\LinkItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base class for data locators.
 */
class DataManager implements DataManagerInterface, ConfigurableInterface, PluginFormInterface {

  use StringTranslationTrait;

  /**
   * Data manager configuration key.
   */
  const CONFIG_KEY = 'genoring.data_manager';

  /**
   * Default data manager configuration.
   *
   * @var array
   */
  public static $defaultConfiguration = [
    'upload_uri' => 'private://upload',
    'data_uri' => 'private://genoring',
    'download_uri' => 'private://download',
    'data_registry_uri' => 'private://genoring/genoring_registry.tsv',
  ];

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * The data locator plugin logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * The data manager configuration.
   *
   * @var array
   */
  protected $configuration;

  /**
   * Default data field definition array.
   *
   * @var array
   * @see https://www.drupal.org/docs/drupal-apis/entity-api/fieldtypes-fieldwidgets-and-fieldformatters
   */
  protected $dataFieldDefinitions = [
    'types' => [
      'boolean',
      'comment',
      'datetime',
      'daterange',
      'decimal',
      'email',
      'entity_reference',
      'file',
      'float',
      'image',
      'integer',
      'link',
      'list_float',
      'list_integer',
      'list_string',
      'string',
      'string_long',
      'telephone',
      'text',
      'text_long',
      'text_with_summary',
      'timestamp',
    ],
    'form_element' => [
      'boolean' => [
        '#type' => 'checkbox',
      ],
      'comment' => [
        // @todo Needs to be adjusted.
        '#type' => 'textarea',
        '#rows' => 5,
        '#attributes' => ['class' => ['js-text-full', 'text-full']],
      ],
      'datetime' => [
        '#type' => 'datetime',
        '#date_year_range' => '1902:2037',
      ],
      'daterange' => [
        // @todo Needs to be adjusted.
        '#type' => 'container',
        'from' => [
          '#type' => 'textfield',
          '#size' => 60,
          '#maxlength' => 255,
        ],
        'to' => [
          '#type' => 'textfield',
          '#size' => 60,
          '#maxlength' => 255,
        ],
      ],
      'decimal' => [
        '#type' => 'number',
        '#step' => 0.1,
      ],
      'email' => [
        '#type' => 'email',
        '#size' => 60,
        '#maxlength' => Email::EMAIL_MAX_LENGTH,
      ],
      'entity_reference' => [
        /*
        @todo Needs to be adjusted.
        @code
        '#type' => 'entity_autocomplete',
        '#target_type' => ...,
        '#selection_handler' => ...,
        '#selection_settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
        ],
        '#validate_reference' => FALSE,
        '#maxlength' => 1024,
        '#size' => 60,
        @endcode
        */
        '#type' => 'textfield',
        '#size' => 60,
        '#maxlength' => 255,
      ],
      'file' => [
        // @todo Needs to be adjusted.
        '#type' => 'textfield',
        '#size' => 60,
        '#maxlength' => 255,
      ],
      'float' => [
        '#type' => 'number',
        '#step' => 'any',
      ],
      'image' => [
        // @todo Needs to be adjusted.
        '#type' => 'textfield',
        '#size' => 60,
        '#maxlength' => 255,
      ],
      'integer' => [
        '#type' => 'number',
      ],
      'link' => [
        // @todo Needs to be adjusted.
        '#type' => 'url',
        '#size' => 100,
        '#maxlength' => 2048,
      ],
      'list_float' => [
        // @todo Needs to be adjusted.
        '#type' => 'textarea',
        '#rows' => 5,
        '#attributes' => ['class' => ['js-text-full', 'text-full']],
      ],
      'list_integer' => [
        // @todo Needs to be adjusted.
        '#type' => 'textarea',
        '#rows' => 5,
        '#attributes' => ['class' => ['js-text-full', 'text-full']],
      ],
      'list_string' => [
        // @todo Needs to be adjusted.
        '#type' => 'textarea',
        '#rows' => 5,
        '#attributes' => ['class' => ['js-text-full', 'text-full']],
      ],
      'string' => [
        '#type' => 'textfield',
        '#size' => 60,
        '#maxlength' => 255,
      ],
      'string_long' => [
        // @todo Needs to be adjusted.
        '#type' => 'textarea',
        '#rows' => 5,
        '#attributes' => ['class' => ['js-text-full', 'text-full']],
      ],
      'telephone' => [
        // @todo Needs to be adjusted.
        '#type' => 'textfield',
        '#size' => 60,
        '#maxlength' => 255,
      ],
      'text' => [
        '#type' => 'textarea',
        '#rows' => 5,
        '#attributes' => ['class' => ['js-text-full', 'text-full']],
      ],
      'text_long' => [
        '#type' => 'textarea',
        '#rows' => 5,
        '#attributes' => ['class' => ['js-text-full', 'text-full']],
      ],
      'text_with_summary' => [
        '#type' => 'textarea',
        '#rows' => 5,
        '#attributes' => ['class' => ['js-text-full', 'text-full']],
      ],
      'timestamp' => [
        // @todo Needs to be adjusted.
        '#type' => 'textfield',
        '#size' => 60,
        '#maxlength' => 255,
      ],
    ],
    'field_config' => [
      'boolean' => [
        'label' => 'Boolean value',
        'description' => '',
        'field_type' => 'boolean',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [
          'on_label' => 'On',
          'off_label' => 'Off',
        ],
      ],
      'comment' => [
        'label' => 'Comment',
        'description' => '',
        'field_type' => 'comment',
        'required' => FALSE,
        'translatable' => FALSE,
        'settings' => [
          'default_mode' => 1,
          'per_page' => 50,
          'anonymous' => 0,
          'form_location' => TRUE,
          'preview' => 1,
        ],
        'default_value' => [],
        'default_value_callback' => '',
      ],
      'datetime' => [
        'label' => 'Datetime',
        'description' => '',
        'field_type' => 'datetime',
        'required' => FALSE,
        'translatable' => FALSE,
        'settings' => [],
        'default_value' => [],
        'default_value_callback' => '',
      ],
      'daterange' => [
        'label' => 'Date Range',
        'description' => '',
        'field_type' => 'daterange',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [],
      ],
      'decimal' => [
        'label' => 'Decimal',
        'description' => '',
        'field_type' => 'decimal',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [
          'min' => NULL,
          'max' => NULL,
          'prefix' => '',
          'suffix' => '',
        ],
      ],
      'email' => [
        'label' => 'Email',
        'description' => '',
        'field_type' => 'email',
        'required' => FALSE,
        'translatable' => FALSE,
        'settings' => [],
        'default_value' => [],
        'default_value_callback' => '',
      ],
      'entity_reference' => [
        'label' => 'Entity Reference',
        'description' => '',
        'field_type' => 'entity_reference',
        'required' => FALSE,
        'translatable' => FALSE,
        'settings' => [
          'handler' => 'default:node',
          'handler_settings' => [
            'target_bundles' => [
              'page' => 'page',
            ],
            'sort' => [
              'field' => '_none',
              'direction' => 'ASC',
            ],
            'auto_create' => FALSE,
            'auto_create_bundle' => '',
          ],
        ],
        'default_value' => [],
        'default_value_callback' => '',
      ],
      'file' => [
        'label' => 'File',
        'description' => '',
        'field_type' => 'file',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [
          'handler' => 'default:file',
          'handler_settings' => [],
          'file_directory' => '[date:custom:Y]-[date:custom:m]',
          'file_extensions' => 'txt',
          'max_filesize' => '',
          'description_field' => FALSE,
        ],
      ],
      'float' => [
        'label' => 'Float',
        'description' => '',
        'field_type' => 'float',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [
          'min' => NULL,
          'max' => NULL,
          'prefix' => '',
          'suffix' => '',
        ],
      ],
      'image' => [
        'label' => 'Image',
        'description' => '',
        'field_type' => 'image',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [
          'handler' => 'default:file',
          'handler_settings' => [],
          'file_directory' => '[date:custom:Y]-[date:custom:m]',
          'file_extensions' => 'png gif jpg jpeg webp',
          'max_filesize' => '',
          'max_resolution' => '',
          'min_resolution' => '',
          'alt_field' => TRUE,
          'alt_field_required' => TRUE,
          'title_field' => FALSE,
          'title_field_required' => FALSE,
          'default_image' => [
            'alt' => '',
            'title' => '',
            'width' => NULL,
            'height' => NULL,
          ],
        ],
      ],
      'integer' => [
        'label' => 'Integer',
        'description' => '',
        'field_type' => 'integer',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [
          'min' => NULL,
          'max' => NULL,
          'prefix' => '',
          'suffix' => '',
        ],
      ],
      'link' => [
        'label' => 'Link',
        'description' => '',
        'field_type' => 'link',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [
          'title' => 1,
          'link_type' => LinkItemInterface::LINK_GENERIC,
        ],
      ],
      'list_float' => [
        'label' => 'List (float)',
        'description' => '',
        'field_type' => 'list_float',
        'required' => FALSE,
        'translatable' => FALSE,
        'settings' => [],
        'default_value' => [],
        'default_value_callback' => '',
      ],
      'list_integer' => [
        'label' => 'List (integer)',
        'description' => '',
        'field_type' => 'list_integer',
        'required' => FALSE,
        'translatable' => FALSE,
        'settings' => [],
        'default_value' => [],
        'default_value_callback' => '',
      ],
      'list_string' => [
        'label' => 'List (string)',
        'description' => '',
        'field_type' => 'list_string',
        'required' => FALSE,
        'translatable' => FALSE,
        'settings' => [],
        'default_value' => [],
        'default_value_callback' => '',
      ],
      'string' => [
        'label' => 'String value',
        'description' => '',
        'field_type' => 'string',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [],
      ],
      'string_long' => [
        'label' => 'string_long',
        'description' => '',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [],
        'field_type' => 'string_long',
      ],
      'telephone' => [
        'label' => 'Telephone',
        'description' => '',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [],
        'field_type' => 'telephone',
      ],
      'text' => [
        'label' => 'Text',
        'description' => '',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [
          'allowed_formats' => [],
        ],
        'field_type' => 'text',
      ],
      'text_long' => [
        'label' => 'Text long',
        'description' => '',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [
          'allowed_formats' => [],
        ],
        'field_type' => 'text_long',
      ],
      'text_with_summary' => [
        'label' => 'Text with summary',
        'description' => '',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [
          'display_summary' => FALSE,
          'required_summary' => FALSE,
          'allowed_formats' => [],
        ],
        'field_type' => 'text_with_summary',
      ],
      'timestamp' => [
        'label' => 'Timestamp',
        'description' => '',
        'required' => FALSE,
        'translatable' => FALSE,
        'default_value' => [],
        'default_value_callback' => '',
        'settings' => [],
        'field_type' => 'timestamp',
      ],
    ],
    'field_storage' => [
      'boolean' => [
        'type' => 'boolean',
        'settings' => [],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'comment' => [
        'type' => 'comment',
        'settings' => [],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'datetime' => [
        'type' => 'datetime',
        'settings' => [],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'daterange' => [
        'type' => 'daterange',
        'settings' => [
          'datetime_type' => 'datetime',
        ],
        'locked' => FALSE,
        'cardinality' => 1,
        'translatable' => TRUE,
        'indexes' => [],
        'persist_with_no_fields' => FALSE,
        'custom_storage' => FALSE,
      ],
      'decimal' => [
        'type' => 'decimal',
        'settings' => [
          'precision' => 10,
          'scale' => 2,
        ],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'email' => [
        'type' => 'email',
        'settings' => [],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'entity_reference' => [
        'type' => 'entity_reference',
        'settings' => [
          'target_type' => 'node',
        ],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'file' => [
        'type' => 'file',
        'settings' => [
          'target_type' => 'file',
          'display_field' => FALSE,
          'display_default' => FALSE,
          'uri_scheme' => 'private',
        ],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'float' => [
        'type' => 'float',
        'settings' => [],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'image' => [
        'type' => 'image',
        'settings' => [
          'target_type' => 'file',
          'display_field' => FALSE,
          'display_default' => FALSE,
          'uri_scheme' => 'public',
          'default_image' => [
            'alt' => '',
            'title' => '',
            'width' => NULL,
            'height' => NULL,
          ],
        ],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'integer' => [
        'type' => 'integer',
        'settings' => [
          'unsigned' => FALSE,
          'size' => 'normal',
        ],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'link' => [
        'type' => 'link',
        'settings' => [],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'list_float' => [
        'type' => 'list_float',
        'settings' => [
          'allowed_values' => [],
          'allowed_values_function' => '',
        ],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'list_integer' => [
        'type' => 'list_integer',
        'settings' => [
          'allowed_values' => [],
          'allowed_values_function' => '',
        ],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'list_string' => [
        'type' => 'list_string',
        'settings' => [
          'allowed_values' => [],
          'allowed_values_function' => '',
        ],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'string' => [
        'type' => 'string',
        'cardinality' => 1,
        'custom_storage' => FALSE,
        'indexes' => [],
        'locked' => FALSE,
        'persist_with_no_fields' => FALSE,
        'settings' => [
          'max_length' => 255,
          'case_sensitive' => FALSE,
          'is_ascii' => FALSE,
        ],
        'translatable' => TRUE,
      ],
      'string_long' => [
        'type' => 'string_long',
        'settings' => [
          'case_sensitive' => FALSE,
        ],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'telephone' => [
        'type' => 'telephone',
        'settings' => [],
        'locked' => FALSE,
        'cardinality' => 1,
        'translatable' => TRUE,
        'indexes' => [],
        'persist_with_no_fields' => FALSE,
        'custom_storage' => FALSE,
      ],
      'text' => [
        'type' => 'text',
        'settings' => [
          'max_length' => 255,
        ],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'text_long' => [
        'type' => 'text_long',
        'settings' => [],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'text_with_summary' => [
        'type' => 'text_with_summary',
        'settings' => [],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
      'timestamp' => [
        'type' => 'timestamp',
        'settings' => [],
        'cardinality' => 1,
        'translatable' => TRUE,
      ],
    ],
    'view_display' => [
      'boolean' => [
        'type' => 'boolean',
        'label' => 'above',
        'settings' => [
          'format' => 'default',
          'format_custom_FALSE' => '',
          'format_custom_TRUE' => '',
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'comment' => [
        'type' => 'comment_default',
        'label' => 'above',
        'settings' => [
          'view_mode' => 'default',
          'pager_id' => 0,
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'datetime' => [
        'type' => 'datetime_default',
        'label' => 'above',
        'settings' => [
          'timezone_override' => '',
          'format_type' => 'medium',
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'daterange' => [
        'type' => 'daterange_default',
        'label' => 'above',
        'settings' => [
          'timezone_override' => '',
          'format_type' => 'medium',
          'from_to' => 'both',
          'separator' => '-',
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'decimal' => [
        'type' => 'number_decimal',
        'label' => 'above',
        'settings' => [
          'thousand_separator' => '',
          'decimal_separator' => '.',
          'scale' => 2,
          'prefix_suffix' => TRUE,
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'email' => [
        'type' => 'basic_string',
        'label' => 'above',
        'settings' => [],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'entity_reference' => [
        'type' => 'entity_reference_label',
        'label' => 'above',
        'settings' => [
          'link' => TRUE,
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'file' => [
        'type' => 'file_default',
        'label' => 'above',
        'settings' => [
          'use_description_as_link_text' => TRUE,
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'float' => [
        'type' => 'number_decimal',
        'label' => 'above',
        'settings' => [
          'thousand_separator' => '',
          'decimal_separator' => '.',
          'scale' => 2,
          'prefix_suffix' => TRUE,
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'image' => [
        'type' => 'image',
        'label' => 'above',
        'settings' => [
          'image_link' => '',
          'image_style' => '',
          'image_loading' => [
            'attribute' => 'lazy',
          ],
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'integer' => [
        'type' => 'number_integer',
        'label' => 'above',
        'settings' => [
          'thousand_separator' => '',
          'prefix_suffix' => TRUE,
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'link' => [
        'type' => 'link',
        'label' => 'above',
        'settings' => [
          'trim_length' => 80,
          'url_only' => FALSE,
          'url_plain' => FALSE,
          'rel' => '',
          'target' => '',
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'list_float' => [
        'type' => 'list_default',
        'label' => 'above',
        'settings' => [],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'list_integer' => [
        'type' => 'list_default',
        'label' => 'above',
        'settings' => [],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'list_string' => [
        'type' => 'list_default',
        'label' => 'above',
        'settings' => [],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'string' => [
        'type' => 'string',
        'label' => 'inline',
        'settings' => [
          'link_to_entity' => FALSE,
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'string_long' => [
        'type' => 'basic_string',
        'label' => 'above',
        'settings' => [],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'telephone' => [
        'type' => 'basic_string',
        'label' => 'above',
        'settings' => [],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'text' => [
        'type' => 'text_default',
        'label' => 'above',
        'settings' => [],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'text_long' => [
        'type' => 'text_default',
        'label' => 'above',
        'settings' => [],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'text_with_summary' => [
        'type' => 'text_default',
        'label' => 'above',
        'settings' => [],
        'third_party_settings' => [],
        'region' => 'content',
      ],
      'timestamp' => [
        'type' => 'timestamp',
        'label' => 'above',
        'settings' => [
          'date_format' => 'medium',
          'custom_date_format' => '',
          'timezone' => '',
          'tooltip' => [
            'date_format' => 'long',
            'custom_date_format' => '',
          ],
          'time_diff' => [
            'enabled' => FALSE,
            'future_format' => '@interval hence',
            'past_format' => '@interval ago',
            'granularity' => 2,
            'refresh' => 60,
          ],
        ],
        'third_party_settings' => [],
        'region' => 'content',
      ],
    ],
    'form_display' => [
      'boolean' => [
        'type' => 'boolean_checkbox',
        'region' => 'content',
        'settings' => [
          'display_label' => TRUE,
        ],
        'third_party_settings' => [],
      ],
      'comment' => [
        'type' => 'comment_default',
        'region' => 'content',
        'settings' => [],
        'third_party_settings' => [],
      ],
      'datetime' => [
        'type' => 'datetime_default',
        'region' => 'content',
        'settings' => [],
        'third_party_settings' => [],
      ],
      'daterange' => [
        'type' => 'daterange_default',
        'region' => 'content',
        'settings' => [],
        'third_party_settings' => [],
      ],
      'decimal' => [
        'type' => 'number',
        'region' => 'content',
        'settings' => [
          'placeholder' => '',
        ],
        'third_party_settings' => [],
      ],
      'email' => [
        'type' => 'email_default',
        'region' => 'content',
        'settings' => [
          'placeholder' => '',
          'size' => 60,
        ],
        'third_party_settings' => [],
      ],
      'entity_reference' => [
        'type' => 'entity_reference_autocomplete',
        'region' => 'content',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => 60,
          'placeholder' => '',
        ],
        'third_party_settings' => [],
      ],
      'file' => [
        'type' => 'file_generic',
        'region' => 'content',
        'settings' => [
          'progress_indicator' => 'throbber',
        ],
        'third_party_settings' => [],
      ],
      'float' => [
        'type' => 'number',
        'region' => 'content',
        'settings' => [
          'placeholder' => '',
        ],
        'third_party_settings' => [],
      ],
      'image' => [
        'type' => 'image_image',
        'region' => 'content',
        'settings' => [
          'progress_indicator' => 'throbber',
          'preview_image_style' => 'thumbnail',
        ],
        'third_party_settings' => [],
      ],
      'integer' => [
        'type' => 'number',
        'region' => 'content',
        'settings' => [
          'placeholder' => '',
        ],
        'third_party_settings' => [],
      ],
      'link' => [
        'type' => 'link_default',
        'region' => 'content',
        'settings' => [
          'placeholder_url' => '',
          'placeholder_title' => '',
        ],
        'third_party_settings' => [],
      ],
      'list_float' => [
        'type' => 'options_select',
        'region' => 'content',
        'settings' => [],
        'third_party_settings' => [],
      ],
      'list_integer' => [
        'type' => 'options_select',
        'region' => 'content',
        'settings' => [],
        'third_party_settings' => [],
      ],
      'list_string' => [
        'type' => 'options_select',
        'region' => 'content',
        'settings' => [],
        'third_party_settings' => [],
      ],
      'string' => [
        'type' => 'string_textfield',
        'region' => 'content',
        'settings' => [
          'size' => 60,
          'placeholder' => '',
        ],
        'third_party_settings' => [],
      ],
      'string_long' => [
        'type' => 'string_textarea',
        'region' => 'content',
        'settings' => [
          'rows' => 5,
          'placeholder' => '',
        ],
        'third_party_settings' => [],
      ],
      'telephone' => [
        'type' => 'telephone_default',
        'region' => 'content',
        'settings' => [
          'placeholder' => '',
        ],
        'third_party_settings' => [],
      ],
      'text' => [
        'type' => 'text_textfield',
        'region' => 'content',
        'settings' => [
          'size' => 60,
          'placeholder' => '',
        ],
        'third_party_settings' => [],
      ],
      'text_long' => [
        'type' => 'text_textarea',
        'region' => 'content',
        'settings' => [
          'rows' => 5,
          'placeholder' => '',
        ],
        'third_party_settings' => [],
      ],
      'text_with_summary' => [
        'type' => 'text_textarea_with_summary',
        'region' => 'content',
        'settings' => [
          'rows' => 9,
          'summary_rows' => 3,
          'placeholder' => '',
          'show_summary' => FALSE,
        ],
        'third_party_settings' => [],
      ],
      'timestamp' => [
        'type' => 'datetime_timestamp',
        'region' => 'content',
        'settings' => [],
        'third_party_settings' => [],
      ],
    ],
  ];

  /**
   * Constructs a DataManager object.
   *
   * The configuration parameters is expected to contain the external entity
   * type (key ExternalEntityTypeInterface::XNTT_TYPE_PROP), the field name
   * (key 'field_name') and the property name (key 'property_name') this data
   * processor will work on.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   */
  public function __construct(
    TranslationInterface $string_translation,
    ConfigFactoryInterface $config_factory,
    LoggerChannelFactoryInterface $logger_factory,
    array $configuration = [],
  ) {
    $this->setStringTranslation($string_translation);
    $this->configFactory = $config_factory;
    $this->loggerChannelFactory = $logger_factory;
    $this->logger = $this->loggerChannelFactory->get('genoring_data_manager');
    $configuration =
      $configuration
      ?: $this->configFactory->get(static::CONFIG_KEY)->get()
      ?? static::$defaultConfig;
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
  ) {
    return new static(
      $container->get('string_translation'),
      $container->get('config.factory'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return static::$defaultConfiguration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    // @todo Display data locator settings.
    // - List current data locators:
    //   - priority order
    //   - metadata term name
    //   - possible value(s)
    //   - corresponding DataLocator plugin id
    //   - Remove button
    // - Add new data locator for metadata term
    // - Fallback default DataLocator plugin id
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state,
  ) {
    $configuration = $form_state->getValues();
    $this->setConfiguration($configuration);
    $this->configFactory->getEditable(static::CONFIG_KEY)
      ->setData($configuration)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getFile(array $metadata) :?FileInterface {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function locateFilePath(
    array $metadata,
    bool $provide_default = FALSE,
  ) :string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function locateDirectory(
    array $metadata,
    bool $provide_default = FALSE,
  ) :string {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function placeFile(
    FileInterface $file,
    array $metadata,
  ) :FileInterface {
    throw new DataLocatorException('Not implemented');
  }

  /**
   * {@inheritdoc}
   */
  public function placeFiles(array|string $file_list) :array {
    throw new DataLocatorException('Not implemented');
  }

  /**
   * {@inheritdoc}
   */
  public function placePackage(
    string $zip_uri,
    array|string $metadata,
    bool $extract_files = FALSE,
  ) :array {
    throw new DataLocatorException('Not implemented');
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(string|FileInterface $file) :array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function setMetadata(
    string|FileInterface $file,
    array $metadata,
    int $mode = DataManagerInterface::METADATA_REPLACE,
  ) :DataManagerInterface {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function generateFormField(
    string $field_name,
    array $field_definition,
    array $override = [],
  ) :array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function generateFieldConfig(
    string $field_name,
    array $field_definition,
    array $override = [],
  ) :array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function generateFieldStorage(
    string $field_name,
    array $field_definition,
    array $override = [],
  ) :array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function generateFieldViewDisplay(
    string $field_name,
    array $field_definition,
    array $override = [],
  ) :array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function generateFieldFormDisplay(
    string $field_name,
    array $field_definition,
    array $override = [],
  ) :array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function generateFieldMapping(
    string $field_name,
    array $field_definition,
    array $override = [],
  ) :array {
    return [];
  }

}
