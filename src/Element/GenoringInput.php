<?php

namespace Drupal\genoring\Element;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Details;
use Drupal\Core\Render\Element\FormElementInterface;
use Drupal\genoring\Event\DataTypeMetadataEvent;
use Drupal\genoring\Event\FileTypesEvent;
use Drupal\genoring\Event\GenoringEvents;

/**
 * Provides a custom form element for 'genoring_input'.
 *
 * @FormElement("genoring_input")
 */
class GenoringInput extends Details implements FormElementInterface {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    $info = parent::getInfo();
    $info = [
      '#input' => TRUE,
      '#open' => TRUE,
    ] + $info;
    $info['#process'][] = [$class, 'processGenoringInput'];

    return $info;
  }

  /**
   * Process callback for 'mon_champ' element.
   *
   * @parma array $element
   *   A reference to a form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   A reference to the complete form.
   */
  public static function processGenoringInput(
    array &$element,
    FormStateInterface $form_state,
    array &$complete_form,
  ) {
    $element_id = ($element['#attributes']['id'] ??= uniqid('gi', TRUE));
    // Set up the form element for this widget.
    $element += [
      '#type' => 'fieldset',
      // '#markup' => 'TEST 14',
    ];
    $element['#tree'] = TRUE;
    $element['import_method'] = [
      '#type' => 'select',
      '#title' => t('Data Import Method'),
      '#description' => t('Select the method to use to import data files.'),
      '#required' => $element['#required'] ?? FALSE,
      '#options' => [
        'single' => t('Provide a single data file'),
        // 'list' => t('Provide a list of data file URIs in a TSV or YAML file'),
        // 'archive' => t('Provide multiple data files in a zip or tar-gzip/bzip archive'),
      ],
      '#default_value' => 'single',
    ];
    $element['import_source'] = [
      '#type' => 'select',
      '#title' => t('Data Source'),
      '#description' => t('Select how the data file is provided.'),
      '#required' => $element['#required'],
      '#options' => [
        'browser' => t('Upload file from the web browser'),
        'updir' => t('Select file from the upload directory'),
        'uri' => t('Provide a file URI/URL'),
        'dataset' => t('Use a file from an existing dataset'),
      ],
      '#attributes' => [
        'data-genoring-selector' => 'isrc' . $element_id,
      ],
    ];
    // We must wrap the managed_file field into a container to get arround
    // issue https://www.drupal.org/project/drupal/issues/2847425.
    $element['new_data_file_container'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[data-genoring-selector="isrc' . $element_id . '"]' => [
            'value' => 'browser',
          ],
        ],
      ],
      'new_data_file' => [
        '#type' => 'managed_file',
        '#title' => t('Upload new file'),
        '#upload_location' => 'private://upload',
        '#upload_validators' => [
          'FileExtension' => [
          // 'extensions' => $extensions,
          ],
        ],
        '#default_value' => $form_state->getValue('new_data_file', ''),
      ],
    ];

    // Get the list of uploaded files.
    $local_file_options = static::listFiles('private://upload/');
    $element['uploaded_data_file'] = [
      '#type' => 'select',
      '#title' => t('Select an already uploaded file'),
      '#options' => $local_file_options,
      '#empty_option' => t('- None -'),
      '#empty_value' => '',
      '#default_value' => $form_state->getValue('uploaded_data_file', ''),
      '#states' => [
        'visible' => [
          ':input[data-genoring-selector="isrc' . $element_id . '"]' => ['value' => 'updir'],
        ],
      ],
    ];
    $element['uri_data_file'] = [
      '#type' => 'textfield',
      '#title' => t('Enter a file URI/URL'),
      '#default_value' => $form_state->getValue('uri_data_file', ''),
      '#states' => [
        'visible' => [
          ':input[data-genoring-selector="isrc' . $element_id . '"]' => ['value' => 'uri'],
        ],
      ],
    ];

    $element['dataset_data_file'] = [
      '#type' => 'item',
      '#title' => t('File from an existing dataset: not implemented yet.'),
      '#states' => [
        'visible' => [
          ':input[data-genoring-selector="isrc' . $element_id . '"]' => ['value' => 'dataset'],
        ],
      ],
    ];
    $element['file_type'] = [
      '#type' => 'select',
      '#title' => t('File type'),
      '#description' => t('Select the file type.'),
      '#required' => $element['#required'],
      '#options' => static::getFileTypes($element, $form_state),
    ];
    // Add metadata fields according to file type.
    $event = new DataTypeMetadataEvent('fasta', 'file');
    \Drupal::service('event_dispatcher')->dispatch(
      $event,
      GenoringEvents::DATA_TYPE_METADATA
    );
    $metadata_fields = $event->getMetadataFields();
    // @todo Make sure existing metadata fields are not replaced.
    $element['metadata'] = [
      '#type' => 'fieldset',
      '#title' => t('Metadata'),
    ];
    $data_manager = \Drupal::service('genoring.data_manager');
    foreach ($metadata_fields as $field_name => $field_definition) {
      $element['metadata'][$field_name] = $data_manager->generateFormField($field_name, $field_definition);
    }

    // Processing.
    $element['processing'] = [
      '#type' => 'fieldset',
      '#title' => t('Data Processing'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    return NULL;
  }

  /**
   * List supported file types.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   An array of file type options for a 'select' form element.
   */
  protected static function getFileTypes(array &$element, FormStateInterface $form_state) :array {

    $cache_backend = \Drupal::service('cache.default');
    if ($cache = $cache_backend->get('genoring_file_types')) {
      $file_types = $cache->data;
    }
    else {
      // Get supported file types.
      $event = new FileTypesEvent();
      \Drupal::service('event_dispatcher')->dispatch(
        $event,
        GenoringEvents::FILE_TYPES
      );
      $file_types = $event->getFileTypes();
      $cache_backend->set('genoring_file_types', $file_types, CacheBackendInterface::CACHE_PERMANENT);
    }
    $file_type_options = [];
    foreach ($file_types as $file_type => $file_type_definition) {
      $categories = $file_type_definition['categories'];
      if (!empty($categories)) {
        foreach ($categories as $category) {
          // Translate categories here and cast them to string because the
          // "select" form element needs options to be strings in order to work
          // properly, otherwise, objects are treated differently.
          $category = implode('/', array_map(
            function ($cat) {
              // The cat(egory) string comes from a literal string.
              // phpcs:ignore
              return (string) t($cat);
            },
            $category
          ));
          NestedArray::setValue(
            $file_type_options,
            [$category, $file_type],
            // The type definition name comes from a literal string.
            // phpcs:ignore
            (string) t($file_type_definition['name'])
          );
        }
      }
      else {
        $file_type_options[$file_type] = $file_type_definition['name'];
      }
    }

    return $file_type_options;
  }

  /**
   * List available uploaded files.
   *
   * @param string $directory
   *   Path of directory to process.
   * @param bool $include_subdir
   *   If TRUE, include files in sub-directories.
   *
   * @return array
   *   An array of form options for a 'select' form element.
   */
  protected static function listFiles(string $directory, bool $include_subdir = FALSE) :array {
    $file_options = [];
    if ($path = \Drupal::service('file_system')->realpath($directory)) {
      $iterator = new \DirectoryIterator($path);
      foreach ($iterator as $file_info) {
        if ($file_info->isFile()) {
          $file_options[$directory . $file_info->getFilename()] = $file_info->getFilename();
        }
        elseif ($include_subdir && $file_info->isDir()) {
          // @todo Support sub-directories.
          // $file_options += static::listFiles($file_info->getPath());
        }
      }
    }
    return $file_options;
  }

}
