<?php

namespace Drupal\genoring\EventSubscriber;

use Drupal\Core\File\FileExists;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\external_entities\Plugin\ExternalEntities\DataAggregator\GroupAggregator;
use Drupal\genoring\Event\DataModelManagementEvent;
use Drupal\genoring\Event\DataModelsEvent;
use Drupal\genoring\Event\DataTypeMetadataEvent;
use Drupal\genoring\Event\FileTypesEvent;
use Drupal\genoring\Event\GenoringEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * GenoRing event subscriber to handle default behaviors.
 */
class GenoringEventsSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Constructs a GenoringEventsSubscriber object.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(TranslationInterface $string_translation) {
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[GenoringEvents::FILE_TYPES][] = 'genoringFileTypes';
    $events[GenoringEvents::DATA_TYPE_METADATA][] = 'genoringDataTypeMetadata';
    $events[GenoringEvents::DATA_MODELS][] = 'genoringDataModels';
    $events[GenoringEvents::DATA_MODEL_MANAGEMENT][] = 'genoringDataModelManagement';
    return $events;
  }

  /**
   * Add default data models.
   *
   * Check available files and provide default form elements for default data
   * models.
   *
   * @param \Drupal\genoring\Event\DataModelsEvent $event
   *   The event.
   */
  public function genoringDataModels(DataModelsEvent $event) {
    $definitions['assembly'] = [
      'label' => $this->t('Genome Assembly'),
      'description' => $this->t('A genome assembly.'),
      'fields' => [
        'genome_version' => [
          'type' => 'string',
          'label' => $this->t('Version'),
        ],
        'genome_fasta' => [
          'type' => 'genoring_input',
          'label' => $this->t('FASTA file'),
          'file_type' => 'fasta',
          'tags' => [
            'required' => ['dna', 'sequences'],
          ],
        ],
        'genome_gff' => [
          'type' => 'genoring_input',
          'label' => $this->t('GFF3 file'),
          'file_type' => 'gff3',
          'tags' => [
            'required' => [],
          ],
        ],
      ],
    ];

    $event->mergeDataModelDefinitions($definitions);
  }

  /**
   * Add default data models.
   *
   * Check available files and provide default form elements for default data
   * models.
   *
   * @param array $event
   *   The event.
   */
  public function genoringCompatibleDataModels(array $event) {
    // @todo To remove as obsolete.
    $dataset = $event->getDataSet();

    // By default, GenoRing provides data models for genomes and genes.
    // Files are GFF3 and FASTA.
    $parameters['gff_files'] = [];
    $parameters['fasta_files'] = [];
    foreach ($dataset->getDataFiles() as $data_file) {
      if (preg_match('/\.gff3?$/i', $data_file->getFilename())) {
        // We got a GFF3 file.
        $parameters['gff_files'][$data_file->getFileUri()] = $data_file->getFilename();
      }
      elseif (preg_match('/\.(?:fa(?:a|sta)|fna)$/i', $data_file->getFilename())) {
        // We got a FASTA file.
        $parameters['fasta_files'][$data_file->getFileUri()] = $data_file->getFilename();
      }
    }

    $definitions = [];
    // We need sequences for the genome and gene models.
    if (!empty($parameters['fasta_files'])) {

      // Genome model definition.
      // @todo Add more metadata than just the name and the version.
      $definitions['genome'] = [
        'title' => $this->t('Genome'),
        'description' => $this->t('An organism genome with its DNA sequences and gene positions.'),
        'form' => $this->getGenomeForm($parameters),
        'validate' => [[$this, 'validateGenomeForm']],
        'submit' => [],
        'storage_clients' => [
          $this->generateStorageClientConfig(
            'xntttsv',
            [
              'root' => \Drupal::state()->get('genoring_data_uri', 'private://genoring'),
              'headers' => 'comment',
              'normalize_headers' => 0,
              'comment_prefix' => '#',
              'separator' => "\t",
              'quoted' => 1,
              'generate_id' => 0,
            ],
            [
              // Users are not allowed to edit this TSV.
              'mode' => GroupAggregator::STORAGE_CLIENT_MODE_READONLY,
            ]
          ),
        ],
        'fields' => [
          'genoring_genome_fasta' => $this->generateFieldDefinition(
            'string',
            'genoring_genome_fasta',
            ['settings' => ['max_length' => 2048]],
            [
              'label' => 'Genome FASTA',
              'description' => 'The FASTA file holding the genome.',
              'required' => TRUE,
            ]
          ),
          // @todo File path in textfields should be turned into file fields.
          'genoring_genome_gff3' => $this->generateFieldDefinition(
            'string',
            'genoring_genome_gff3',
            ['settings' => ['max_length' => 2048]],
            [
              'label' => 'Gene locations',
              'description' => 'The GFF3 file holding the genome gene locations.',
              'required' => TRUE,
            ]
          ),
          'genoring_genome_version' => $this->generateFieldDefinition(
            'string',
            'genoring_genome_version',
            [],
            [
              'label' => 'Genome version',
              'description' => 'The current version of the genome.',
              'required' => TRUE,
            ]
          ),
        ],
        'mapping' => [
          'id' => $this->generateFieldMapping('id'),
          'title' => $this->generateFieldMapping('name'),
          'genoring_genome_version' => $this->generateFieldMapping('version'),
          'genoring_genome_fasta' => $this->generateFieldMapping('fasta'),
          'genoring_genome_gff3' => $this->generateFieldMapping('gff3'),
        ],
      ];

      // Gene model definition.
      // @todo To complete. A TSV file should be added to provide sequence aliases.
      $definitions['gene'] = [
        'title' => $this->t('Genes'),
        'description' => $this->t('A set of genes.'),
        'form' => $this->getGeneForm($parameters),
        'validate' => [],
        'submit' => [],
        'storage_clients' => [
          $this->generateStorageClientConfig(
            // @todo We should use a GFF storage client once implemented.
            'xntttsv',
            [
              'root' => \Drupal::state()->get('genoring_data_uri', 'private://genoring'),
              'headers' => 'comment',
              'normalize_headers' => 1,
              'comment_prefix' => '#',
              'separator' => "\t",
              'quoted' => 0,
              'generate_id' => 1,
            ]
          ),
        ],
        'fields' => [
          'genoring_seqid' => $this->generateFieldDefinition(
            'string',
            'genoring_seqid',
            [],
            [
              'label' => 'SeqID',
              'description' => 'The ID of the landmark used to establish the coordinate system for the current feature.',
              'required' => TRUE,
            ]
          ),
        ],
        'mapping' => [
          'id' => $this->generateFieldMapping('_id'),
          'title' => $this->generateFieldMapping(
            'preg_replace("/^.*Name=([^;]+).*$/","$1",$.f8)',
            [],
            [
              'config' => [
                'property_mappings' => ['value' => ['id' => 'stringjsonpath']],
              ],
            ]
          ),
          /*
          'genoring_attributes' => $this->generateFieldMapping(
            'explode(";",$.f8)',
            [],
            [
              'config' => [
                'property_mappings' => ['value' => ['id' => 'stringjsonpath']],
              ],
            ]
          ),
          'genoring_end' => $this->generateFieldMapping('f4', 'default'),
          'genoring_phase' => $this->generateFieldMapping('f7'),
          'genoring_score' => $this->generateFieldMapping('f5', 'default'),
          */
          'genoring_seqid' => $this->generateFieldMapping('f0'),
          /*
          'genoring_source' => $this->generateFieldMapping('f1'),
          'genoring_start' => $this->generateFieldMapping('f3', 'default'),
          'genoring_strand' => $this->generateFieldMapping('f6'),
          'genoring_type' => $this->generateFieldMapping('f2'),
          */
        ],
      ];
    }
    $this->mergeDataModels($event, $definitions);
  }

  /**
   * Provides a form for the genome model.
   *
   * @param array $parameters
   *   Form parameters such as options for FASTA and GFF3 file selection.
   */
  public function getGenomeForm(array $parameters) :array {
    $form = [
      'genome_name' => [
        '#type' => 'textfield',
        '#title' => $this->t('Genome name'),
        // '#required' => TRUE,
      ],
      'genome_version' => [
        '#type' => 'textfield',
        '#title' => $this->t('Genome version'),
        // '#required' => TRUE,
      ],
      'fasta_file' => [
        '#type' => 'select',
        '#title' => $this->t('Genome DNA sequences'),
        '#description' => $this->t('The FASTA file is expected to contain chromosomes or scaffold or other related DNA sequences of the genome.'),
        '#options' => $parameters['fasta_files'],
        // '#required' => TRUE,
      ],
    ];
    if (!empty($parameters['gff_files'])) {
      $form['gff3_file'] = [
        '#type' => 'select',
        '#title' => 'GFF3 file with gene positions',
        '#options' => $parameters['gff_files'],
        '#required' => FALSE,
        '#empty_option' => $this->t('- Select -'),
      ];
    }
    return $form;
  }

  /**
   * Genome form validation.
   */
  public function validateGenomeForm(array &$form, FormStateInterface $form_state) {
    // Get selected FASTA file.
    if (($trigger = $form_state->getTriggeringElement())
        && preg_match('/add_model_(\w+)/', $trigger['#name'], $matches)
    ) {
      // @todo Checks...
      $dataset = $form_state->getFormObject()->getEntity();
      $model_id = $matches[1];
      $model_definition = $form_state->get('models.' . $model_id);
      if (!empty($model_definition)) {
        $fasta_file = $form_state->getValue(['data_models', $model_id, 'fasta_file']);
        $gff3_file = $form_state->getValue(['data_models', $model_id, 'gff3_file']);
        // @todo Generate a TSV file and use it instead of file storages.
        $file_uri = \Drupal::state()->get('genoring_data_uri', 'private://genoring') . '/' . $dataset->id() . '-' . $model_id . '.tsv';
        $data = '';
        if (file_exists($file_uri)) {
          $data = file_get_contents($file_uri);
          // @todo Get columns and replace genome line.
        }
        else {
          $data =
            "#id\tname\tversion\tfasta\tgff3\n"
            . $dataset->id()
            . "\t"
            . $form_state->getValue(['data_models', $model_id, 'genome_name'], $dataset->id())
            . "\t"
            . $form_state->getValue(['data_models', $model_id, 'genome_version'], '')
            . "\t"
            . ($fasta_file ?? '')
            . "\t"
            . ($gff3_file ?? '')
            . "\n";
        }
        $file_repository = \Drupal::service('file.repository');
        $file = $file_repository->writeData($data, $file_uri, FileExists::Replace);
        $file->setPermanent();
        $file->save();
        // @todo [0] should be turned into 'genome_file' once we got a data model aggregator.
        $model_definition['storage_clients'][0]['config']['structure'] = str_replace(\Drupal::state()->get('genoring_data_uri', 'private://genoring') . '/', '', $file->getFileUri());
        $form_state->set('models.' . $model_id, $model_definition);
      }
    }
  }

  /**
   * Provides a form for the gene model.
   *
   * @param array $parameters
   *   Form parameters such as options for FASTA and GFF3 file selection.
   */
  public function getGeneForm(array $parameters) :array {
    $form = [
      'cds_fasta_file' => [
        '#type' => 'select',
        '#title' => $this->t('Gene CDS (DNA) sequences'),
        '#description' => $this->t('The FASTA file is expected to contain a set of CDS sequences.'),
        '#options' => $parameters['fasta_files'],
        '#required' => FALSE,
        '#empty_option' => $this->t('- Select -'),
      ],
      'protein_fasta_file' => [
        '#type' => 'select',
        '#title' => $this->t('Gene protein (amino-acid) sequences'),
        '#description' => $this->t('The FASTA file is expected to contain a set of protein sequences.'),
        '#options' => $parameters['fasta_files'],
        '#required' => FALSE,
        '#empty_option' => $this->t('- Select -'),
      ],
      // @todo Manage Chado data source if available, to fetch genes and proteins.
      // @todo Add genome selection if genome models are available.
      'gff3_file' => [
        '#type' => 'select',
        '#title' => $this->t('Gene locations'),
        '#description' => $this->t('The GFF3 file is expected to contain gene location matching the FASTA sequence names.'),
        '#options' => $parameters['gff_files'],
        '#required' => FALSE,
        '#empty_option' => $this->t('- Select -'),
      ],
    ];
    return $form;
  }

  /**
   * Provides default action to available data models.
   *
   * @param \Drupal\genoring\Event\DataModelManagementEvent $event
   *   The event.
   */
  public function genoringDataModelManagement(DataModelManagementEvent $event) {
    // @todo Implement.
    $data_model = $event->getDataModel();
    $management_form = $event->getManagementForm();
    $event->setManagementForm($management_form);
  }

  /**
   * Provides default file types.
   *
   * Supported:
   * - Sequences/alignments:
   *   - FASTA files in various flavors
   * - Media:
   *   - PNG images
   *   - JPEG images
   * - Others:
   *   - GFF3 files
   *   - TSV files.
   *
   * // Not added yet: CSV, FASTQ, SAM, BAM, CRAM, stockholm (.sto, .stk), VCF,
   * // BCF, GTF, GFF2, Newick (.nwk, .nhx, .tree), NEXUS, PHYLIP (.phy, .ph),
   * // Clustal, PhyloXML, XML, YAML, TXT, MD, RDF, DOC(x), XLS(x), LOG, PPT,
   * // PDF, EMBL, EMBOSS, BLAST outputs, BED, SVG, TIFF, GIF, JSON, ZIP, TGZ,
   * // GZ, BZ, TBZ, TAR, LaTeX (.tex), SQL, DIFF, ...
   *
   * @param \Drupal\genoring\Event\FileTypesEvent $event
   *   The event.
   */
  public function genoringFileTypes(FileTypesEvent $event) {
    // @todo To complete.
    $event->addFileTypes(
      [
        // Sequences and alignments.
        'fasta' => [
          'extensions' => ['fa', 'fas', 'fasta', 'ffn', 'fna', 'frn', 'faa', 'seq'],
          'name' => 'FASTA sequences',
          'categories' => [['Sequences']],
          'description' => 'One or more nucleic or amino acid sequences in FASTA format.',
          'mime' => 'text/plain',
          'plugins' => [],
          'specs' => 'https://en.wikipedia.org/wiki/FASTA_format',
        ],
        'fasta_alignment' => [
          'extensions' => ['fa', 'fas', 'fasta', 'ffn', 'fna', 'frn', 'faa', 'seq', 'fsa'],
          'name' => 'FASTA alignment',
          'categories' => [['Alignments']],
          'description' => 'Multiple nucleic or amino acid sequence alignment in FASTA format.',
          'mime' => 'text/plain',
          'plugins' => [],
          'specs' => 'https://en.wikipedia.org/wiki/FASTA_format',
        ],
        // Media.
        'jpeg' => [
          'extensions' => ['jpg', 'jpeg'],
          'name' => 'JPEG image',
          'categories' => [['Media', 'Images']],
          'description' => 'Joint Photographic Experts Group image.',
          'mime' => 'image/jpeg',
          'plugins' => [],
        ],
        'png' => [
          'extensions' => ['png'],
          'name' => 'PNG image',
          'categories' => [['Media', 'Images']],
          'description' => 'Portable Network Graphics image.',
          'mime' => 'image/png',
          'plugins' => [],
        ],
        'text' => [
          'extensions' => ['txt', 'md'],
          'name' => 'Text file',
          'categories' => [['Media', 'Documents']],
          'description' => 'Basic text file or markdown.',
          'mime' => 'text/plain',
          'plugins' => [],
        ],
        // Others.
        'gff3' => [
          'extensions' => ['gff', 'gff3'],
          'name' => 'GFF3',
          'categories' => [['Others']],
          'description' => 'Generic Feature Format Version 3 (GFF3).',
          'mime' => 'text/plain',
          'plugins' => [],
          'specs' => 'https://github.com/The-Sequence-Ontology/Specifications/blob/master/gff3.md',
        ],
        'tsv' => [
          'extensions' => ['tsv', 'tab'],
          'name' => 'TSV',
          'categories' => [['Others']],
          'description' => 'Tabulation-separated value file.',
          'mime' => 'text/tab-separated-values',
          'plugins' => [],
        ],
        'tsv_germplasm' => [
          'extensions' => ['tsv', 'tab'],
          'name' => 'Germplasm TSV',
          'categories' => [['Others']],
          'description' => 'Tabulation-separated value file containing germplasm data.',
          'mime' => 'text/tab-separated-values',
          'plugins' => [],
        ],
      ]
    );
  }

  /**
   * Event handler for data type metadata event.
   *
   *  Generates metadata fields according to the data type.
   *
   * @param \Drupal\genoring\Event\DataTypeMetadataEvent $event
   *   The event.
   */
  public function genoringDataTypeMetadata(DataTypeMetadataEvent $event) {
    if ('file' === $event->getCategory()) {
      $event->addMetadataField(
        'file_creation_date',
        [
          'type' => 'datetime',
          'label' => $this->t('File creation date'),
        ],
      );
    }
  }

}
