<?php

namespace Drupal\genoring\Controller;

/**
 * GenoRing dashboard page.
 */

use Drupal\Core\Controller\ControllerBase;

/**
 * Class Dashboard.
 *
 * GenoRing dashboard controller class.
 */
class Dashboard extends ControllerBase {

  /**
   * Generate a render array with our templated content.
   *
   * @return array
   *   A render array.
   */
  public function mainPage() {
    $content = [
      '#theme' => 'genoring_dashboard',
      '#title' => t('GenoRing Dashboard.'),
    ];
    return $content;
  }

}
