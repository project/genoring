<?php

namespace Drupal\genoring\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\file\FileInterface;

/**
 * GenoRing Dataset entity interface.
 *
 * In GenoRing, a dataset is a local directory containing a set of data files or
 * symbolic links to data files and a metadata file for that dataset.
 * A dataset can be used by one or more data models. Files shared amonst
 * multiple datasets are added to a dataset using a symbolic link while those
 * files are stored in a parallel structure managed by data locators.
 *
 * Each file of the dataset has associated metadata. The metadata can be
 * retrieved using the GenoRing metadata manager (service).
 *
 * @ingroup genoring
 */
interface DatasetInterface extends ConfigEntityInterface {

  /**
   * Returns the list of URIs of file managed by this dataset.
   *
   * @return string[]
   *   A list of file URIs or an empty list if no files are managed.
   */
  public function getDataFileUris() :array;

  /**
   * Add a file URI to this dataset.
   *
   * @param string $file_uri
   *   File URI.
   *
   * @return self
   *   Current instance.
   */
  public function addDataFileUri(string $file_uri) :self;

  /**
   * Remove a file URI from this dataset.
   *
   * @param string $file_uri
   *   File URI.
   *
   * @return self
   *   Current instance.
   */
  public function removeDataFileUri(string $file_uri) :self;

  /**
   * Returns the list of files managed by this dataset keyed by their URIs.
   *
   * @return \Drupal\file\FileInterface[]
   *   A list of files or an empty list if no files are managed.
   */
  public function getDataFiles() :array;

  /**
   * Add a file to this dataset.
   *
   * @param \Drupal\file\FileInterface $file
   *   A managed file instance.
   *
   * @return self
   *   Current instance.
   */
  public function addDataFile(FileInterface $file) :self;

  /**
   * Remove a file from this dataset.
   *
   * @param \Drupal\file\FileInterface $file
   *   A managed file instance.
   *
   * @return self
   *   Current instance.
   */
  public function removeDataFile(FileInterface $file) :self;

  /**
   * Returns the list of data models used by this dataset.
   *
   * @return string[]
   *   A list of external entity type names.
   */
  public function getDataModels() :array;

  /**
   * Sets the list of data models used by this dataset.
   *
   * @param string[] $model_names
   *   New list of data models (external entity type machine names) to use.
   *
   * @return self
   *   Current instance.
   */
  public function setDataModels(array $model_names) :self;

  /**
   * Returns the list of data models used by this dataset.
   *
   * The returned list contains external entity types.
   *
   * @return ExternalEntityTypeInterface[]
   *   A list of external entity types.
   */
  public function getDataModelInstances() :array;

}
