<?php

namespace Drupal\genoring\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\file\FileInterface;

/**
 * GenoRing Dataset entity implementation.
 *
 * @ingroup genoring
 *
 * @ConfigEntityType(
 *   id = "genoring_dataset",
 *   label = @Translation("GenoRing dataset"),
 *   admin_permission = "administer genoring",
 *   handlers = {
 *     "access" = "Drupal\genoring\DatasetAccessController",
 *     "list_builder" = "Drupal\genoring\Controller\DatasetListBuilder",
 *     "form" = {
 *       "add" = "Drupal\genoring\Form\DatasetAddForm",
 *       "edit" = "Drupal\genoring\Form\DatasetEditForm",
 *       "delete" = "Drupal\genoring\Form\DatasetDeleteForm"
 *     }
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "edit-form" = "/genoring/dataset/{genoring_dataset}",
 *     "delete-form" = "/genoring/dataset/{genoring_dataset}/delete"
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "label",
 *     "fileUris",
 *     "dataModels"
 *   }
 * )
 */
class Dataset extends ConfigEntityBase implements DatasetInterface {

  /**
   * The dataset ID.
   *
   * @var string
   */
  public $id;

  /**
   * The dataset UUID.
   *
   * @var string
   */
  public $uuid;

  /**
   * The dataset label.
   *
   * @var string
   */
  public $label;

  /**
   * The data file uri list.
   *
   * @var string[]
   */
  public array $fileUris = [];

  /**
   * The data file list keyed by file URIs.
   *
   * @var \Drupal\file\FileInterface[]
   */
  public array $files = [];

  /**
   * The data models (machine name) in use.
   *
   * @var string[]
   */
  public array $dataModels = [];

  /**
   * The data models (external entity type instances) in use.
   *
   * @var \Drupal\external_entities\Entity\ExternalEntityTypeInterface[]
   */
  public array $dataModelInstances = [];

  /**
   * {@inheritdoc}
   */
  public function getDataFileUris() :array {
    return $this->fileUris;
  }

  /**
   * {@inheritdoc}
   */
  public function addDataFileUri(string $file_uri) :self {
    if (!in_array($file_uri, $this->fileUris)) {
      $this->fileUris[] = $file_uri;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeDataFileUri(string $file_uri) :self {
    $this->fileUris = array_filter(
      $this->fileUris,
      function ($uri) {
        return $uri !== $file_uri;
      }
    );
    unset($this->files[$file_uri]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataFiles() :array {
    // Check if some files are not loaded.
    $uri_to_load = [];
    foreach ($this->fileUris as $file_uri) {
      if (empty($this->files[$file_uri])) {
        $uri_to_load[] = $file_uri;
      }
    }
    if (!empty($uri_to_load)) {
      $file_storage = \Drupal::entityTypeManager()->getStorage('file');
      $files = $file_storage->loadByProperties([
        'uri' => $uri_to_load,
      ]);
      foreach ($files as $file_id => $file) {
        $this->files[$file->uri->first()->get('value')->getValue()] = $file;
      }
    }
    return $this->files;
  }

  /**
   * {@inheritdoc}
   */
  public function addDataFile(FileInterface $file) :self {
    $file_uri = $file->uri->first()->get('value')->getValue();
    $this->addDataFileUri($file_uri);
    if (empty($this->files[$file_uri])) {
      $this->files[$file_uri] = $file;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function removeDataFile(FileInterface $file) :self {
    $file_uri = $file->uri->first()->get('value')->getValue();
    $this->removeDataFileUri($file_uri);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataModels() :array {
    return $this->dataModels;
  }

  /**
   * {@inheritdoc}
   */
  public function setDataModels(array $model_names) :self {
    $this->dataModels = $model_names;
    // Clear instances for reloading.
    $this->dataModelInstances = [];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDataModelInstances() :array {
    if (empty($this->dataModelInstances) && !empty($this->dataModels)) {
      $xntt_storage = \Drupal::entityTypeManager()->getStorage('external_entity_type');
      $model_instances =
        $xntt_storage->loadMultiple(
          array_map(
            function ($model_name) {
              return $this->id() . '__' . $model_name;
            },
            $this->dataModels
          )
        );
      // We add the length of '__'.
      $dataset_prefix_len = strlen($this->id()) + 2;
      $this->dataModelInstances = array_combine(
        array_map(
          function ($model_id) use ($dataset_prefix_len) {
            return substr($model_id, $dataset_prefix_len);
          },
          array_keys($model_instances)
        ),
        array_values($model_instances)
      );
    }
    return $this->dataModelInstances;
  }

}
