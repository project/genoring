<?php

namespace Drupal\genoring\DataProcessor;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Interface for data processor plugins.
 *
 * The data processor can be used to pre-process data, report issues and
 * automatically fix issues when it is possible.
 */
interface DataProcessorInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Issue 'error' status.
   */
  const STATUS_ERROR = 'error';

  /**
   * Issue 'warning' status.
   */
  const STATUS_WARNING = 'warning';

  /**
   * Issue 'notice' status.
   */
  const STATUS_NOTICE = 'notice';

  /**
   * Issue 'info' status.
   */
  const STATUS_INFO = 'info';

  /**
   * Returns the administrative label for this data processor plugin.
   *
   * @return string
   *   The data processor administrative label.
   */
  public function getLabel() :string;

  /**
   * Get the list of supported file types.
   *
   * @return array
   *   The list of supported file types machine names (as provided by the
   *   FileTypesEvent event).
   */
  public function getSupportedFileTypes() :array;

  /**
   * Returns the administrative description for this data processor plugin.
   *
   * @return string
   *   The data processor administrative description.
   */
  public function getDescription() :string;

  /**
   * Returns an array of detected issues in the given data.
   *
   * @param string $raw_data
   *   The raw data.
   * @param array &$context
   *   A context array that contains processing informations with possible
   *   intermediates and may contain pre-processed data. The context array is
   *   passed to each data processors during both data checking and data
   *   processing so data processors could share pre-processed data to avoid
   *   reprocessing several time the same things. For instance, raw data of a
   *   FASTA file could be pre-processed by a first data processor that would
   *   return the parsed file into an array using the key
   *   `$context['fasta_array']`. Other data processors that are aware of that
   *   key could either parse the FASTA file if the key is not there, or use the
   *   parsed content directly and alter it as needed for the next processors.
   *   Data-processor plugin specific context data should be stored under its
   *   plugin identifier key to avoid side effects with other plugins.
   *
   * @return array
   *   An array with the following possible keys set: 'error', 'warning',
   *   'notice' and 'info'. Each of these keys contains a list of issue
   *   messages (strings). 'error' means the issue cannot be fixed and the data
   *   cannot be processed, 'warning' means the issue can be fixed
   *   automatically, 'notice' means a possible issue has been detected but can
   *   be ignored, and 'info' can be used to provide information and statistics
   *   about the data.
   *   Example:
   *   @code
   *   [
   *     static::STATUS_ERROR => [
   *       'Entry 15 has invalid data.',
   *       'Entry 31 has invalid characters in its name.',
   *     ],
   *     static::STATUS_WARNING => [
   *       'Entry 96 has upper case characters that will be lowercased.',
   *       'Entry 111 has upper case characters that will be lowercased.',
   *       'Entry 206 has an extra line break that will be removed.',
   *     ],
   *     static::STATUS_INFO => ['300 entries were processed.'],
   *   ]
   *   @endcode
   *   See DataProcessorInterface::STATUS_* constants.
   */
  public function checkData(
    string $raw_data,
    array &$context = [],
  ) :array;

  /**
   * Returns the processed data.
   *
   * @param string $raw_data
   *   The initial raw data.
   * @param array &$context
   *   A context array that contains processing informations with possible
   *   intermediates and may contain pre-processed data. The context array is
   *   passed to each data processors during both data checking and data
   *   processing so data processors could share pre-processed data to avoid
   *   reprocessing several time the same things. For instance, raw data of a
   *   FASTA file could be pre-processed by a first data processor that would
   *   return the parsed file into an array using the key
   *   `$context['fasta_array']`. Other data processors that are aware of that
   *   key could either parse the FASTA file if the key is not there, or use the
   *   parsed content directly and alter it as needed for the next processors.
   *
   *   Also, a $context['processings'] key is reserved to hold information about
   *   each data processing done. Each processing step information should be
   *   stored there in the format:
   *   @code
   *   'processings' => [
   *     <data processor plugin id> => [
   *       <step number in reverse order> => [
   *         'name' => <name of the step>,
   *         'code' => <short machine name of the step>,
   *         'data' => <processed result (except for step 0)>,
   *       ],
   *     ],
   *   ],
   *   @endcode
   *   For instance, consider a data processor "x" that has 3 steps: the first
   *   one lowercases the data, the second one replaces non-word characters
   *   except semi-columns by underscores and the last one replaces semi-columns
   *   by new lines. If that data processors has to process the
   *   "Some Data;To be processed!" string, and "intermediate" generation has
   *   been requested in its config, it will return the following string with
   *   the following $context:
   *   @code
   *   // Returned string:
   *   "some_data\nto_be_processed_";
   *   // With $context:
   *   [
   *     'x' => [
   *       [
   *         'name' => 'Semi-columns to newlines',
   *         'code' => 'sc2nl',
   *         // This data is already returned by the method.
   *       ],
   *       [
   *         'name' => 'Non-word cleaning',
   *         'code' => 'nwcl',
   *         'data' => "some_data;to_be_processed_",
   *       ],
   *       [
   *         'name' => 'Lowercase the data',
   *         'code' => 'low',
   *         'data' => "some data;to be processed!",
   *       ],
   *     ],
   *   ],
   *   @endcode
   *
   * @return string
   *   The processed data.
   */
  public function processData(
    string $raw_data,
    array &$context = [],
  ) :string;

}
