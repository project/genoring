<?php

namespace Drupal\genoring\DataProcessor;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin type manager for data processors.
 *
 * @see \Drupal\genoring\DataProcessor\DataProcessorInterface
 */
class DataProcessorManager extends DefaultPluginManager {

  /**
   * Constructs a DataProcessorManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct(
      'Plugin/GenoRing/DataProcessor',
      $namespaces,
      $module_handler,
      '\Drupal\genoring\DataProcessor\DataProcessorInterface',
      'Drupal\genoring\Annotation\DataProcessor'
    );

    $this->alterInfo('genoring_data_processor_info');
    $this->setCacheBackend(
      $cache_backend,
      'genoring_data_processor',
      ['genoring_data_processor']
    );
  }

  /**
   * Returns data processors matching the given file type.
   *
   * @param string $type
   *   The file type.
   *
   * @return array
   *   A list of data processor identifiers that can process the given file
   *   type data.
   */
  public function getDataProcessorsForType(string $type) :array {
    $data_processor_ids = [];
    $data_processor_definitions = $this->getDefinitions();
    foreach ($data_processor_definitions as $definition) {
      if (in_array($type, $definition['supportedTypes'])) {
        $data_processor_ids[] = $definition['id'];
      }
    }
    return array_values(array_unique($data_processor_ids));
  }

}
