<?php

namespace Drupal\genoring\Plugin\GenoRing\DataProcessor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\genoring\DataProcessor\DataProcessorBase;

/**
 * GFF3 data processor.
 *
 * @DataProcessor(
 *   id = "gff3",
 *   label = @Translation("GFF3 Processor"),
 *   description = @Translation("Cleans up a GFF3 file."),
 *   supportedTypes = {"gff", "gff3"}
 * )
 */
class Gff3Processor extends DataProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    $form['fix_position'] = [
      '#type' => 'checkbox',
      '#title' => 'Fix feature positions (not impemented yet)',
      '#default_value' => $form_state->getValue('fix_position', 1),
      '#return_value' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function checkData(
    string $raw_data,
    array &$context = [],
  ) :array {
    return [
      static::STATUS_NOTICE => ['No checks were implemented yet.'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function processData(
    string $raw_data,
    array &$context = [],
  ) :string {
    // @todo Implement.
    \Drupal::messenger()->addMessage('GFF3 Processor: not implemented yet.');
    return $raw_data;
  }

}
