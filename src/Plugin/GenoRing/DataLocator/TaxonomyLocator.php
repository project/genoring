<?php

namespace Drupal\genoring\Plugin\GenoRing\DataLocator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\genoring\DataLocator\DataLocatorBase;
use Drupal\genoring\Taxonomy\TaxonomyInterface;

/**
 * Taxonomy locator.
 *
 * @DataLocator(
 *   id = "taxonomy",
 *   label = @Translation("Taxonomy locator"),
 *   description = @Translation("Data locator used to place files in taxonomic directories."),
 *   defaultSupportedMetadata = {"$.taxonomy"}
 * )
 */
class TaxonomyLocator extends DataLocatorBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'mode' => 'code',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    $configuration = $this->getConfiguration();
    $form['mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Directory naming:'),
      '#required' => TRUE,
      '#options' => [
        'code' => $this->t('Use species codes ("0OTHER" when not available and "0MULTI" for data with multiple species).'),
        'taxid' => $this->t('Use the taxonomic identifier (taxid) used on ENA and NCBI ("other" when not available and "multi" for data with multiple species)).'),
        'name' => $this->t('Use the taxonomic name (all lower-cased, and characters that are not numeric dash, dot or word characters are replaces by underscores).'),
      ],
      '#default_value' => $form_state->getValue(
        'mode',
        $configuration['mode']
        ?: $this->defaultConfiguration()['mode']
      ),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function locateUri(
    array $metadata,
    string $base_uri = '',
  ) :string {
    $uri = $base_uri;
    [$stream, $path, $filename] = $this->explodeUri($uri);
    $configuration = $this->getConfiguration();
    if (!empty($metadata['type']) && !empty($metadata['taxonomy'])) {
      $taxonomy = \Drupal::service('genoring.taxonomy');
      $taxonomy_dir = '';
      if (!is_array($metadata['taxonomy']) || (1 == count($metadata['taxonomy']))) {
        if (is_array($metadata['taxonomy'])) {
          reset($metadata['taxonomy']);
          $metadata['taxonomy'] = current($metadata['taxonomy']);
        }
        if ('code' == $configuration['mode']) {
          $taxonomy_dir = $taxonomy->getSpeciesCode($metadata['taxonomy']) ?: TaxonomyInterface::SPECIES_CODE_UNKNOWN;
        }
        elseif ('taxid' == $configuration['mode']) {
          $taxonomy_dir = $taxonomy->getTaxonomyId($metadata['taxonomy']);
        }
        elseif ('name' == $configuration['mode']) {
          $taxonomy_dir = $taxonomy->getNormalizedSpeciesName($metadata['taxonomy']);
        }
        else {
          $this->logger->warning('Invalid taxonomy directory naming mode (' . ($configuration['mode'] ?? '') . ').');
        }
      }
      else {
        if ('code' == $configuration['mode']) {
          $taxonomy_dir = TaxonomyInterface::SPECIES_CODE_MULTI;
        }
        elseif ('taxid' == $configuration['mode']) {
          $taxonomy_dir = TaxonomyInterface::SPECIES_NAME_MULTI;
        }
        elseif ('name' == $configuration['mode']) {
          $taxonomy_dir = TaxonomyInterface::SPECIES_NAME_MULTI;
        }
        else {
          $this->logger->warning('Invalid taxonomy directory naming mode (' . ($configuration['mode'] ?? '') . ').');
        }
      }

      if ('file' === $metadata['type']) {
        $uri = $stream . $path . $taxonomy_dir . '/' . $filename;
      }
      elseif ('directory' === $metadata['type']) {
        $uri = $stream . $path . $taxonomy_dir . '/';
      }
    }
    return $uri;
  }

}
