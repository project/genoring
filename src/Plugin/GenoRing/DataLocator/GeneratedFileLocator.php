<?php

namespace Drupal\genoring\Plugin\GenoRing\DataLocator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\genoring\DataLocator\DataLocatorBase;

/**
 * Generated file locator.
 *
 * @DataLocator(
 *   id = "generated",
 *   label = @Translation("Generated file locator"),
 *   description = @Translation("Data locator used to place files that have been generated from others."),
 *   defaultSupportedMetadata = {"$.processor"}
 * )
 */
class GeneratedFileLocator extends DataLocatorBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'tool_uri' => '',
      'tool_subdir' => 1,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    $configuration = $this->getConfiguration();
    $form['tool_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Parent output directory:'),
      '#description' => $this->t('Parent directory to store data processor outputs. This can be empty.'),
      '#required' => FALSE,
      '#default_value' => $form_state->getValue(
        'tool_uri',
        $configuration['tool_uri']
        ?: static::defaultConfiguration()['tool_uri']
      ),
    ];
    $form['tool_subdir'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Store outputs in sub-directory by data processor names'),
      '#required' => FALSE,
      '#default_value' => $form_state->getValue(
        'tool_subdir',
        $configuration['tool_subdir']
        ?: static::defaultConfiguration()['tool_subdir']
      ),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function locateUri(
    array $metadata,
    string $base_uri = '',
  ) :string {
    $uri = $base_uri;
    [$stream, $path, $filename] = $this->explodeUri($uri);
    $configuration = $this->getConfiguration();
    if (!empty($metadata['type']) && !empty($metadata['processor'])) {
      $processor_dir = strtolower(
        trim(
          preg_replace('/\W/', '_', $metadata['processor']),
          '_'
        )
      );
      if ('file' === $metadata['type']) {
        $uri = $stream . $path . $processor_dir . '/' . $filename;
      }
      elseif ('directory' === $metadata['type']) {
        $uri = $stream . $path . $processor_dir . '/';
      }
    }
    return $uri;
  }

}
