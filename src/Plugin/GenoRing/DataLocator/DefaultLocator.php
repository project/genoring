<?php

namespace Drupal\genoring\Plugin\GenoRing\DataLocator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\genoring\DataLocator\DataLocatorBase;
use Drupal\genoring\DataManager\DataManager;
use Drupal\genoring\Exception\DataLocatorException;

/**
 * Default data locator.
 *
 * @DataLocator(
 *   id = "default",
 *   label = @Translation("Default data locator"),
 *   description = @Translation("Data locator used as default to place files in the configured default data path."),
 *   defaultSupportedMetadata = {}
 * )
 */
class DefaultLocator extends DataLocatorBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'data_uri' => DataManager::$defaultConfiguration['data_uri'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    $configuration = $this->getConfiguration();
    $form['data_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base data URI:'),
      '#description' => $this->t('The trailing slash is not required.'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => t('private://genoring'),
      ],
      '#default_value' => $form_state->getValue(
        'data_uri',
        $configuration['data_uri']
        ?: static::defaultConfiguration()['data_uri']
      ),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function locateUri(
    array $metadata,
    string $base_uri = '',
  ) :string {
    $uri = $base_uri;
    // Make sure we got a non-empty base URI.
    if (empty($uri)) {
      $configuration = $this->getConfiguration();
      $uri =
        $configuration['data_uri']
        ?? DataManager::$defaultConfiguration['data_uri'];
    }
    // Make sure we got a stream.
    if (FALSE === strpos($uri, '://')) {
      if (FALSE === strpos($configuration['data_uri'], '://')) {
        $uri =
          DataManager::$defaultConfiguration['data_uri']
          . '/'
          . ltrim($uri, '/');
      }
      else {
        $uri =
          rtrim($configuration['data_uri'], '/')
          . '/'
          . ltrim($uri, '/');
      }
    }
    // Validate full URI.
    try {
      [$stream, $path, $filename] = $this->explodeUri($uri);
      $uri = $stream . $path . $filename;
    }
    catch (DataLocatorException $exception) {
      // Unsupported (invalid) URI. Return what was provided.
      $uri = $base_uri;
    }

    return $uri;
  }

}
