<?php

namespace Drupal\genoring\Plugin\GenoRing\DataLocator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\genoring\DataLocator\DataLocatorBase;

/**
 * Metadata locator.
 *
 * @DataLocator(
 *   id = "metadata",
 *   label = @Translation("Metadata locator"),
 *   description = @Translation("Data locator used to place metadata files."),
 *   defaultSupportedMetadata = {"$.format=metadata"}
 * )
 */
class MetadataLocator extends DataLocatorBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'data_uri' => '.metadata',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    $configuration = $this->getConfiguration();
    $form['data_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Metadata suffix:'),
      '#description' => $this->t('The suffix appended to data file or name of the sub-directory holding the metadata files.'),
      '#required' => TRUE,
      '#attributes' => [
        'placeholder' => t('.metadata'),
      ],
      '#default_value' => $form_state->getValue(
        'data_uri',
        $configuration['data_uri']
        ?: $this->defaultConfiguration()['data_uri']
      ),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function locateUri(
    array $metadata,
    string $base_uri = '',
  ) :string {
    $uri = $base_uri;
    [$stream, $path, $filename] = $this->explodeUri($uri);
    $configuration = $this->getConfiguration();
    if (!empty($metadata['type'])) {
      if ('file' === $metadata['type']) {
        $uri = $stream . $path . $configuration['data_uri'] . '/' . $filename . $configuration['data_uri'];
      }
      elseif ('directory' === $metadata['type']) {
        $uri = $stream . $path . $configuration['data_uri'] . '/';
      }
    }
    return $uri;
  }

}
