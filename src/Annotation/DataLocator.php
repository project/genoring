<?php

namespace Drupal\genoring\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a data locator.
 *
 * @see \Drupal\genoring\DataLocator\DataLocatorManager
 * @see plugin_api
 *
 * @Annotation
 */
class DataLocator extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-friendly name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The default supported metadata structure this plugin is designed for.
   *
   * An empty array means the plugin supports any metadata. Otherwise, each
   * element of the array is a string describing a possible metadata match (ie.
   * joined using a "or" logic). Each metadata match string contains one or more
   * rules separated by comas (",") that must all be matched by the metadata
   * (ie. joined using a "and" logic). Each rule is a JSON path to a key that
   * must be existing in the metadata array. If the JSON path is followed by an
   * equal sign, one or more values can be provided and must match the metadata
   * value corresponding to the matching key. Multiple values can be specified
   * in square brakets and the must all have a match in the correspondig
   * metadata array values (ie. "and" logic, for "or" logic, use multiple
   * metadata match).
   *
   * Ex.:
   * - match any metadata:
   *   []
   * - match only metadata with a taxonomy:
   *   ['$.taxonomy']
   * - match only metadata with the "Musa" taxonomy:
   *   ['$.taxonomy=Musa']
   * - match only metadata with the "Musa" or "Ensete" taxonomy:
   *   [
   *     '$.taxonomy=Musa',
   *     '$.taxonomy=Ensete',
   *   ]
   * - match any data file that contain protein sequence alignments:
   *   ['$.type=file,$.content=[protein,alignments]']
   * - match data files that are fasta or phylip protein sequence alignments:
   *   [
   *     '$.type=file,$.format=fasta,$.content=[protein,alignments]',
   *     '$.type=file,$.format=phylip,$.content=[protein,alignments]',
   *   ]
   *
   * @var array
   */
  public $defaultSupportedMetadata;

}
