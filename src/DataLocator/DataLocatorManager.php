<?php

namespace Drupal\genoring\DataLocator;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin type manager for data locators.
 *
 * @see \Drupal\genoring\DataLocator\DataLocatorInterface
 */
class DataLocatorManager extends DefaultPluginManager {

  /**
   * Constructs a DataLocatorManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
  ) {
    parent::__construct(
      'Plugin/GenoRing/DataLocator',
      $namespaces,
      $module_handler,
      '\Drupal\genoring\DataLocator\DataLocatorInterface',
      'Drupal\genoring\Annotation\DataLocator'
    );

    $this->alterInfo('genoring_data_locator_info');
    $this->setCacheBackend(
      $cache_backend,
      'genoring_data_locator',
      ['genoring_data_locator']
    );
  }

}
