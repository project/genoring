<?php

namespace Drupal\genoring\DataLocator;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Interface for data locator plugins.
 *
 * A data locator plugin is used to locate or place files in a file structure
 * according to its metadata.
 */
interface DataLocatorInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Returns the administrative label for this data locator plugin.
   *
   * @return string
   *   The data locator administrative label.
   */
  public function getLabel() :string;

  /**
   * Returns the administrative description for this data locator plugin.
   *
   * @return string
   *   The data locator administrative description.
   */
  public function getDescription() :string;

  /**
   * Returns the metadata structure supported by default by this plugin.
   *
   * The metadata structure only need to contain supported keys and values. If
   * a special key with no specific values could trigger the use of the plugin,
   * the value should be set to NULL. If alternative metadata structures could
   * trigger the use of the plugin, the returned array should only contain
   * numeric keys at its first level with a 0 key (for alternative mode
   * detection).
   *
   * Ex.:
   * - a plugin that supports any files with organism information:
   *   @code
   *   [
   *     // Note: while this key is set to NULL here, it will match any value
   *     // on a metadata array that has this key existing (even empty).
   *     'organism' => NULL,
   *   ]
   *   @endcode
   * - a plugin that supports assembly files:
   *   @code
   *   [
   *     'data_model' => 'assembly',
   *   ]
   *   @endcode
   * - a plugin that supports any FASTA or GFF files:
   *   @code
   *   [
   *     [
   *       'file_format' => 'fasta',
   *     ],
   *     [
   *       'file_format' => 'gff',
   *     ],
   *   ]
   *   @endcode
   * - a plugin that only supports nucleic and amino acid FASTA files of
   *   pangenomes:
   *   @code
   *   [
   *     [
   *       'file_format' => 'fasta',
   *       'file_type' => 'amino acid sequences',
   *       'data_model' => 'pangenome',
   *     ],
   *     [
   *       'file_format' => 'fasta',
   *       'file_type' => 'amino acid sequences',
   *       'data_model' => 'pangenome',
   *     ],
   *   ]
   *   @endcode
   *
   * @return array
   *   The data locator default supported metadata structure.
   */
  public function getDefaultSupportedMetadata() :array;

  /**
   * Returns the URI that should correspond to the given metadata.
   *
   * @param array $metadata
   *   A metadata array. For 'file', the $metadat array is expected to have a
   *   'type' key to 'file', and for directories a 'type' to 'directory'.
   * @param string $base_uri
   *   Base URI. For directories, the provided base URI must end by a slash.
   *
   * @return string
   *   The URI corresponding to the given metadata according to the given base
   *   URI. The returned URI should include the given base URI but a completely
   *   different URI with a different protocol is also valid. If the plugin
   *   returns a directory name, it must end by a slash, otherwise it will be
   *   used as a part of the file or directory name. If the metadata does not
   *   contain the elements required by the plugin, the base URI should be
   *   returned.
   */
  public function locateUri(
    array $metadata,
    string $base_uri = '',
  ) :string;

}
