<?php

namespace Drupal\genoring\DataLocator;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\genoring\Exception\DataLocatorException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base class for data locators.
 */
abstract class DataLocatorBase extends PluginBase implements DataLocatorInterface, ConfigurableInterface, PluginFormInterface {

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * The data locator plugin logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * Constructs a DataLocatorBase object.
   *
   * The configuration parameters is expected to contain the external entity
   * type (key ExternalEntityTypeInterface::XNTT_TYPE_PROP), the field name
   * (key 'field_name') and the property name (key 'property_name') this data
   * processor will work on.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The identifier for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   */
  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
  ) {
    $this->setConfiguration($configuration);
    $configuration = $this->getConfiguration();
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setStringTranslation($string_translation);
    $this->loggerChannelFactory = $logger_factory;
    $this->logger = $this->loggerChannelFactory->get('genoring_locator_' . $plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() :string {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() :string {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['description'] ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSupportedMetadata() :array {
    $plugin_definition = $this->getPluginDefinition();
    return $plugin_definition['defaultSupportedMetadata'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state,
  ) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state,
  ) {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * Explodes a URI into 3 pieces: stream, path and file name.
   *
   * @param string $uri
   *   A file or directory URI. Directory URI must end by a slash otherwise the
   *   last sub-directory will be considered as a file. If the stream is omited
   *   or incomplete, 'public://' will be used.
   *
   * @return array
   *   An array with 3 values: the stream, a path and file name. The returned
   *   stream is never empty and always ends with '://'. The returned path may
   *   be empty if no path part was found, however if it is not empty, it will
   *   always end by a slash. The returned file name may be empty if no file
   *   name was found.
   */
  protected function explodeUri(string $uri) {
    if (preg_match('~^((?:\w*(?=:))?:?//(?!/)|)/?((?:[^:/]|(?<!/)/)*(?<!/)/|)([^/:]+|)$~', $uri, $matches)) {
      if (3 >= length($matches[1])) {
        $matches[1] = 'public://';
      }
      return array_slice($matches, 1);
    }
    else {
      throw new DataLocatorException("Invalid URI: '$uri'");
    }
  }

}
