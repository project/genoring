<?php

namespace Drupal\genoring\Taxonomy;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\file\Entity\File;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base class for data locators.
 */
class Taxonomy implements TaxonomyInterface {

  use StringTranslationTrait;

  /**
   * Kingdom codes.
   *
   * @var array
   */
  public static $kingdoms = [
    'A' => 'archaea',
    'B' => 'bacteria',
    'E' => 'eukaryota',
    'V' => 'viruses',
    'O' => 'others',
  ];

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * The data locator plugin logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The HTTP client to fetch the files with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs a Taxonomy object.
   *
   * The Taxonomy object can be used to work with taxonomy data.
   *
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger channel factory.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   A file system service.
   * @param \Drupal\Core\Database\Connection $connection
   *   A database connection.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   A Guzzle client object.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    TranslationInterface $string_translation,
    LoggerChannelFactoryInterface $logger_factory,
    FileSystemInterface $file_system,
    Connection $connection,
    ClientInterface $http_client,
    EntityTypeManager $entity_type_manager,
  ) {
    $this->setStringTranslation($string_translation);
    $this->loggerChannelFactory = $logger_factory;
    $this->logger = $this->loggerChannelFactory->get('genoring_taxonomy');
    $this->fileSystem = $file_system;
    $this->connection = $connection;
    $this->httpClient = $http_client;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
  ) {
    return new static(
      $container->get('string_translation'),
      $container->get('logger.factory'),
      $container->get('file_system'),
      $container->get('database'),
      $container->get('http_client'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function loadTaxonomy() {
    $speclist_path = static::UNIPROT_SPECIES_CV_LOCAL_URI;
    $data = '';
    // First, check if a local copy of the 'speclist.txt' file is available.
    if (!file_exists($speclist_path)) {
      // Not available, try to download it.
      try {
        $response = $this->httpClient->request(
          'GET',
          static::UNIPROT_SPECIES_CV_URL
        );
        $data = $response->getBody() . '';
        $speclist_dir = strrchr($speclist_path, '/', TRUE);
        $this->fileSystem->prepareDirectory($speclist_dir, FileSystemInterface::MODIFY_PERMISSIONS | FileSystemInterface::CREATE_DIRECTORY);
        if (!$this->fileSystem->saveData($data, $speclist_path, FileExists::Replace)) {
          $this->logger->error('Failed to save ' . $speclist_path);
        }
      }
      catch (GuzzleException $exception) {
        $this->logger->error('Error in taxonomy loading. Message: ' . $exception->getMessage());
      }
    }
    else {
      // Available, load raw data.
      $data = file_get_contents($speclist_path);
    }

    // Load data if we got some.
    if (!empty($data)) {
      $species_data = $this->parseSpeciesList($data);

      if (!empty($species_data)) {
        try {
          // Clear previous data.
          $this->connection->truncate('genoring_taxonomy')->execute();
          try {
            // Load new data.
            $query = $this->connection
              ->upsert('genoring_taxonomy')
              ->fields([
                'taxid',
                'code',
                'name',
                'normalized',
                'common',
                'synonyms',
                'kingdom',
              ])
              ->key('taxid');
            foreach ($species_data as $entry) {
              $query->values($entry);
            }
            $query->execute();
          }
          catch (\Exception $exception) {
            $this->logger->error('Error in taxonomy loading. Failed to load new data: ' . $exception->getMessage());
          }
        }
        catch (\Exception $exception) {
          $this->logger->error('Error in taxonomy loading. Failed to clear previous data (no data loaded): ' . $exception->getMessage());
        }
      }
      else {
        // Unable to parse speclist.
        $this->logger->error('Error in taxonomy loading. Unable to parse species list data.');
        // Try to remove local file to have it reloaded.
        $this->removeTaxonomyFile();
      }
    }
    else {
      $this->logger->error('Error in taxonomy loading. Failed to get data to load.');
    }
  }

  /**
   * Remove the local copy of the taxonomy file (species list).
   */
  public function removeTaxonomyFile() {
    $speclist_path = static::UNIPROT_SPECIES_CV_LOCAL_URI;
    $files = $this->entityTypeManager
      ->getStorage('file')
      ->loadByProperties(['uri' => $speclist_path]);
    if (!empty($files)) {
      // Managed by Drupal.
      $file = reset($files);
      if ($file instanceof File) {
        $file->delete();
      }
      else {
        $this->logger->error(
          'Failed to remove invlaid species list file (' . $speclist_path . ').'
        );
      }
    }
    elseif (file_exists($speclist_path)) {
      // Probably added manually.
      unlink($speclist_path);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function parseSpeciesList(string $raw_data) :array {
    $speclist = [];
    $lines = explode("\n", $raw_data);
    do {
      $line = array_shift($lines);
      // Try to find next entry.
      if (preg_match('/^([0-9A-Z]{3,5})\s+([ABEVO])\s+(\d+):\s+(.+)$/', $line, $matches)) {
        // Got an entry, get its rest.
        [$full, $code, $kingdom, $taxid] = $matches;
        $name_data = [$matches[4]];
        while (!empty($lines) && preg_match('/^\s{16,}(\S+)\s*$/', $lines[0], $name_matches)) {
          $name_data[] = $name_matches[1];
          $line = array_shift($lines);
        }
        $name = '';
        $common = '';
        $synonyms = [];
        foreach ($name_data as $name_data_element) {
          if ((3 > strlen($name_data_element))
              || ('=' !== $name_data_element[1])
          ) {
            // Invalid line.
          }
          elseif ('N' === $name_data_element[0]) {
            $name = substr($name_data_element, 2);
          }
          elseif ('C' === $name_data_element[0]) {
            $common = substr($name_data_element, 2);
          }
          elseif ('S' === $name_data_element[0]) {
            $synonyms[] = substr($name_data_element, 2);
          }
        }
        $speclist[] = [
          'taxid' => (int) $taxid,
          'code' => $code,
          'name' => $name,
          'normalized' => $this->normalizeName($name),
          'common' => $common,
          'synonyms' => implode(',', $synonyms),
          'kingdom' => $kingdom,
        ];
      }
    } while (!empty($lines));

    return $speclist;
  }

  /**
   * {@inheritdoc}
   */
  public function normalizeName(string $name) :string {
    return strtolower(trim(preg_replace('/[^0-9A-Za-z]+/', '_', $name), '_'));
  }

  /**
   * {@inheritdoc}
   *
   *   @code
   *   [
   *     'taxid' => <the taxon node>,
   *     'code' => <the species code>,
   *     'name' => <the organism official name>,
   *     'normalized' => <the organism normalized name>,
   *     'common' => <the organism common name>,
   *     'synonyms' => <the organism common name>,
   *     'kingdom' => <the organism kingdom>,
   *   ]
   *   @endcode
   */
  public function getSpecies(?string $species = NULL) :array {
    $query = $this->connection
      ->select('genoring_taxonomy', 'gt')
      ->fields('gt', [
        'taxid',
        'code',
        'name',
        'normalized',
        'common',
        'synonyms',
        'kingdom',
      ]);
    if (!empty($species)) {
      if (preg_match('/^\d+$/', $species)) {
        // We git a taxid.
        $query->condition('taxid', $species);
      }
      elseif (preg_match('/^[0-9A-Z]{3,5}$/', $species)) {
        // We git a species code.
        $query->condition('code', $species);
      }
      else {
        // We got a species name.
        $query->condition('normalized', $this->normalizeName($species));
      }
    }

    $results = $query->execute();
    $species_list = [];
    foreach ($results as $record) {
      $species_list[$record->code] = [
        'taxid' => $record->taxid,
        'code' => $record->code,
        'name' => $record->name,
        'normalized' => $this->normalizeName($record->name),
        'common' => $record->common,
        'synonyms' => empty($record->synonyms) ? [] : explode(',', $record->synonyms),
        'kingdom' => static::$kingdoms[$record->kingdom] ?? 'others',
      ];
    }

    return $species_list;
  }

  /**
   * {@inheritdoc}
   */
  public function getSpeciesCode(string $species) :string {
    return (current($this->getSpecies($species)) ?: ['code' => ''])['code'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSpeciesName(string $species) :string {
    return (current($this->getSpecies($species)) ?: ['name' => ''])['name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getNormalizedSpeciesName(string $species) :string {
    return (current($this->getSpecies($species)) ?: ['normalized' => ''])['normalized'];
  }

  /**
   * {@inheritdoc}
   */
  public function getTaxonomyId(string $species) :int {
    return (current($this->getSpecies($species)) ?: ['taxid' => 0])['taxid'];
  }

}
