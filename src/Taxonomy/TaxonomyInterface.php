<?php

namespace Drupal\genoring\Taxonomy;

/**
 * Interface for taxonomy.
 *
 * A data manager is used to locate data and manage uploaded files. It is also
 * used to manage data fields and metadata.
 */
interface TaxonomyInterface {

  /**
   * UniProt species codes controlled vocabulary file URL.
   */
  const UNIPROT_SPECIES_CV_URL = 'https://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/docs/speclist.txt';

  /**
   * UniProt species codes controlled vocabulary local file URI.
   */
  const UNIPROT_SPECIES_CV_LOCAL_URI = 'public://genoring/speclist.txt';

  /**
   * Species code used for unknown species.
   */
  const SPECIES_CODE_UNKNOWN = '0OTHER';

  /**
   * Species name used for unknown species.
   */
  const SPECIES_NAME_UNKNOWN = 'other';

  /**
   * Species code used for multiple species.
   */
  const SPECIES_CODE_MULTI = '0MULTI';

  /**
   * Species name used for multiple species.
   */
  const SPECIES_NAME_MULTI = 'multiple';

  /**
   * Load taxonomy data into the system.
   *
   * If previous data exists, it is cleared.
   */
  public function loadTaxonomy();

  /**
   * Parses UniProt speclist.txt file into an associative array.
   *
   * @param string $raw_data
   *   The raw speclist.txt file content.
   *
   * @return array
   *   An array of entry with the following structure:
   *   @code
   *   [
   *     'taxid' => <int: the taxon node identifier>,
   *     'code' => <string: the species code>,
   *     'name' => <string: the organism official name>,
   *     'normalized' => <string: the organism normalized name>,
   *     'common' => <string: the organism common name>,
   *     'synonyms' => <string: the coma-separated list of synonyms>,
   *     'kingdom' => <string: the organism 1-letter kingdom>,
   *   ]
   *   @endcode
   */
  public function parseSpeciesList(string $raw_data) :array;

  /**
   * Normalize a given name string.
   *
   * @param string $name
   *   The name to nomalize.
   *
   * @return string
   *   The normalized name containing only alpha-numeric characters with no
   *   consecutive underscores and which does not start or end by an underscore.
   */
  public function normalizeName(string $name) :string;

  /**
   * Returns UniProt species data.
   *
   * @param string|null $species
   *   Can be either a species (normalized/unnormalized) name, a species code or
   *   a numeric taxonomy identifier (auto-detected).
   *
   * @return string
   *   Returns an associative array of species data keyed by their 5-letter
   *   species code. Each species data is an associative array of the form:
   *   @code
   *   [
   *     'taxid' => <the taxon node as an integer>,
   *     'code' => <the 5-letters species code>,
   *     'name' => <the organism scientific name>,
   *     'normalized' => <the organism normalized name>,
   *     'common' => <the organism common name>,
   *     'synonyms' => [<the organism synonyms>],
   *     'kingdom' => <the organism kingdom code>,
   *   ]
   *   @endcode
   *   If $species is empty, all organisms are returned. If $species is not
   *   empty, only the matching organism or an empty array is returned.
   */
  public function getSpecies(?string $species = NULL) :array;

  /**
   * Returns the UniProt species code corresponding to the given species.
   *
   * @param string $species
   *   Can be either a species (normalized/unnormalized) name, a species code or
   *   a numeric taxonomy identifier (auto-detected).
   *
   * @return string
   *   The corresponding 5-letter species code or an empty string if no match.
   */
  public function getSpeciesCode(string $species) :string;

  /**
   * Returns the official species name corresponding to the given species.
   *
   * @param string $species
   *   Can be either a species (normalized/unnormalized) name, a species code or
   *   a numeric taxonomy identifier (auto-detected).
   *
   * @return string
   *   The corresponding organism name or an emtpy string if no match.
   */
  public function getSpeciesName(string $species) :string;

  /**
   * Returns the normalized species name corresponding to the given species.
   *
   * @param string $species
   *   Can be either a species (normalized/unnormalized) name, a species code or
   *   a numeric taxonomy identifier (auto-detected).
   *
   * @return string
   *   The corresponding normalized organism name or an emtpy string if no
   *   match. The normalized name only contains lower case alpha-numeric letters
   *   and underscores, does not contain consecutive underscores, and does not
   *   start or end by an underscore.
   */
  public function getNormalizedSpeciesName(string $species) :string;

  /**
   * Returns the taxonomy number corresponding to the given organism node.
   *
   * @param string $species
   *   Can be either a species (normalized/unnormalized) name, a species code or
   *   a numeric taxonomy identifier (auto-detected).
   *
   * @return int
   *   The corresponding taxon node number if one or 0 if not found.
   */
  public function getTaxonomyId(string $species) :int;

}
