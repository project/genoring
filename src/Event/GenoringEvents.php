<?php

namespace Drupal\genoring\Event;

/**
 * Defines events for the genoring module.
 *
 * @see \Drupal\Core\Config\ConfigCrudEvent
 */
final class GenoringEvents {

  /**
   * Name of the event fired to list file types.
   *
   * This event allows modules to add their own file types.
   *
   * @Event
   */
  const FILE_TYPES = 'genoring.file_types';

  /**
   * Name of the event fired to list metadata fields for a given type.
   *
   * This event allows modules to add their own metadata fields for a type.
   *
   * @Event
   */
  const DATA_TYPE_METADATA = 'genoring.data_type_metadata';

  /**
   * Name of the event fired to list available data models.
   *
   * This event allows modules to add their own data models or add their fields
   * to given data models.
   *
   * @Event
   */
  const DATA_MODELS = 'genoring.data_models';

  /**
   * Name of the event fired when a data model should be managed.
   *
   * This event allows modules to include their own features on a given data
   * model applyed to a given external entity type.
   *
   * @Event
   */
  const DATA_MODEL_MANAGEMENT = 'genoring.data_model_management';

}
