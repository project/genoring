<?php

namespace Drupal\genoring\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Component\Utility\NestedArray;

/**
 * The Data Models Event.
 *
 * The DataModelsEvent gather information from event handlers on available data
 * models.
 *
 * Each event handler can add new models or complete existing ones.
 */
class DataModelsEvent extends Event {

  /**
   * The data model definition array.
   *
   * The data model definition array holds data models  with current
   * dataset data files. Each key of the array is a data model machine name and
   * the values are associative arrays with the following structure:
   * - title: the title of the data model.
   * - description: the description of the data model.
   * - form: the form structure with the required and optional elements to
   *   create the data model. The form should not contain submit buttons. See
   *   after to handle validate and submit actions.
   * - validate: an array of form validation methods specified like for the
   *   "submit" form element type.
   * - submit: an array of submition methods specified like for the "submit"
   *   form element. These are only for specific field additions as the external
   *   entity creation (ie. data model) will be handled by the main dataset
   *   submit handler. The created external entity will be available through
   *   the form state value 'external_entity'.
   * - storage_clients: an array of external entity storage client definitions.
   *   Each definition follows the same structure of the external entity group
   *   data aggregator storage clients: an 'id' key with the storage client
   *   identifier, a 'config' key with the client configuration and an 'aggr'
   *   key with aggregation settings. See GroupAggregator config schema for
   *   details
   *   (`external_entities.external_entities_aggr_storage_client_settings`).
   * - fields: the array of external entity field definitions for the model
   *   keyed by their machine names. Each field machine name should start by the
   *   "genoring_" prefix to have them fully managed by GenoRing. Each field
   *   definition must have a 'storage' key with a field storage definition
   *   array that will be passed to FieldStorageConfig::create() method and a
   *   'config' key with a field configuration array that will be passed to the
   *   FieldConfig::create() method. Two other keys are optional: 'view_display'
   *   with the field display settings and 'form_display' with the field form
   *   display settings.
   * - mapping: field mapping definitions. See external entities config schema
   *   `external_entities.external_entities_field_mapper_settings`.
   * Event subscribers are responsible to keep the data model structure valid
   * while they may alter other subscribers changes.
   *
   * @var array
   */
  protected $dataModelDefinitions;

  /**
   * Constructs a data model event object.
   */
  public function __construct() {
    $this->dataModelDefinitions = [];
  }

  /**
   * Gets data models.
   *
   * @return array
   *   The data models.
   */
  public function getDataModelDefinitions() :array {
    return $this->dataModelDefinitions;
  }

  /**
   * Add data models.
   *
   * @param array $data_models
   *   The data models to add or complete.
   * @param string $merge_mode
   *   Merge mode. Can be either 'replace' (default) to replace existing
   *   definitions or 'keep' to keep existing definitions and just append
   *   missing elements.
   *
   * @return self
   *   Current instance.
   */
  public function mergeDataModelDefinitions(
    array $data_models,
    string $merge_mode = 'replace',
  ) :self {
    if ('replace' == $merge_mode) {
      // Replace existing definitions.
      $this->dataModelDefinitions = NestedArray::mergeDeep(
        $this->dataModelDefinitions,
        $data_models
      );
    }
    else {
      // "keep" mode.
      $this->dataModelDefinitions = NestedArray::mergeDeep(
        $data_models,
        $this->dataModelDefinitions
      );
    }
    return $this;
  }

}
