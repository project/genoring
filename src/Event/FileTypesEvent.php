<?php

namespace Drupal\genoring\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * The File Types Event.
 *
 * Provides a way to gather all supported file types.
 */
class FileTypesEvent extends Event {

  /**
   * The file types structure.
   *
   * @var array
   *
   * File types are keyed by file type codes and have the following structure:
   * - extensions: an array of file extensions without a dot prefix.
   * - categories: a single category or an array of categories matching the
   *   type. Each category is itself an array that may contain ordered
   *   sub-categories.
   *   Eg.:
   *   @code
   *     // Single category.
   *     ['Sequences', 'FASTA'];
   *     // Multiple categories.
   *     [
   *       ['Sequences', 'NEXUS'],
   *       ['Alignments', 'NEXUS'],
   *       ['Trees', 'NEXUS'],
   *     ];
   *   @endcode
   *   A same file type may belong to multiple categories.
   * - name: the name of the file type (short).
   * - description: the description of the file type. It can use multiple lines.
   * - mime: file (default) mime type.
   * - plugins: an array of data processor plugin identifiers that can process
   *   this type of file.
   * - specs: an optional URL to find file format specifications.
   *
   * Example:
   * @code
   *   [
   *     'fasta' => [
   *       'extensions' => ['fa', 'fas', 'fasta', 'ffn', 'fna', 'seq'],
   *       'name' => 'FASTA sequences',
   *       'categories' => ['Sequences'],
   *       'description' => 'One or more nucleic or amino acid sequences in FASTA format.',
   *       'mime' => 'text/plain',
   *       'plugins' => ['genoring_fasta'],
   *       'specs' => 'https://en.wikipedia.org/wiki/FASTA_format',
   *     ],
   *     'nexus' => [
   *       'extensions' => ['nex', 'nexus', 'nxs'],
   *       'name' => 'Nexus data file',
   *       // Categories "Sequences", "Alignments" and "Trees".
   *       'categories' => [['Sequences'], ['Alignments'], ['Trees']],
   *       'description' => 'Nexus file format can contain sequences, alignments, trees, taxonomic data and more.',
   *       'mime' => 'application/octet-stream',
   *       'plugins' => ['nexus'],
   *       'specs' => 'https://en.wikipedia.org/wiki/Nexus_file',
   *     ],
   *     'jpeg' => [
   *       'extensions' => ['jpg', 'jpeg'],
   *       'name' => 'JPEG image',
   *       // Category "Media" with sub-category "Images".
   *       'categories' => [['Media', 'Images']],
   *       'description' => 'Joint Photographic Experts Group image.',
   *       'mime' => 'image/jpeg',
   *       'plugins' => [],
   *     ],
   *   ];
   * @endcode
   *
   * Note: try to use the same code as other module do for the same type.
   * See GenoringEventsSubscriber::genoringFileTypes() for examples.
   *
   * @see \Drupal\genoring\EventSubscriber\GenoringEventsSubscriber::genoringFileTypes
   */
  protected $fileTypes = [];

  /**
   * Constructs a compatible data model event object.
   *
   * @param array $default
   *   A default array of file types.
   */
  public function __construct(array $default = []) {
    $this->fileTypes = $default;
  }

  /**
   * Gets the file types.
   *
   * @return array
   *   The file types.
   */
  public function getFileTypes() :array {
    return $this->fileTypes;
  }

  /**
   * Gets the list of file extensions.
   *
   * @return array
   *   A sorted list of extensions without dot prefix and no
   *   duplicates.
   */
  public function getFileExtensions() :array {
    $extensions_arrays = array_map(
      function ($file_type) {
        return $file_type['extensions'] ?? [];
      },
      array_values($this->fileTypes)
    );
    $extensions = array_values(
      array_unique(
        array_merge(...$extensions_arrays)
      )
    );
    return $extensions;
  }

  /**
   * Add file types.
   *
   * This method will add the given types to the current structure of types. If
   * some types are already there, it will only add missing values and keep
   * existing ones.
   *
   * @param array $file_types
   *   Add the given types to the structure.
   *
   * @return self
   *   Current instance.
   */
  public function addFileTypes(array $file_types) :self {
    foreach ($file_types as $type => $definition) {
      if (!empty($this->fileTypes[$type])) {
        foreach (['name', 'mime', 'description'] as $field) {
          if (empty($this->fileTypes[$type][$field])
              && !empty($definition[$field])
          ) {
            $this->fileTypes[$type][$field] = $definition[$field];
          }
        }
        foreach (['extensions', 'plugins'] as $field) {
          $this->fileTypes[$type][$field] = array_unique(
            array_merge(
              $this->fileTypes[$type][$field] ?? [],
              $definition[$field] ?? []
            )
          );
        }
        // Merging categories is a bit more tricky as there could be a single
        // category or arrays of categories, and each category may have
        // a specific sub-category.
        $categories = [];
        // Make sure we work with arrays of categories and aggregate them.
        if (!empty($this->fileTypes[$type]['categories'])
            && !is_array($this->fileTypes[$type]['categories'][0])) {
          $categories = array_merge(
            $categories,
            [$this->fileTypes[$type]['categories']]
          );
        }
        else {
          $categories = array_merge(
            $categories,
            $this->fileTypes[$type]['categories'] ?? []
          );
        }
        if (!empty($definition['categories'])
            && !is_array($definition['categories'][0])) {
          $categories = array_merge(
            $categories,
            [$definition['categories']]
          );
        }
        else {
          $categories = array_merge(
            $categories,
            $definition['categories'] ?? []
          );
        }
        // Now remove duplicates. We implode arrays in strings to compare them.
        $categories = array_unique(
          array_map(
            function ($cat) {
              return implode("\t", $cat);
            },
            $categories
          )
        );
        // Turn back category strings into arrays.
        $categories = array_map(
          function ($cat) {
            return explode("\t", $cat);
          },
          $categories
        );
        $this->fileTypes[$type]['categories'] = $categories;
      }
      else {
        $this->fileTypes[$type] = $definition;
      }
    }
    return $this;
  }

}
