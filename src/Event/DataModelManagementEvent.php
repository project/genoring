<?php

namespace Drupal\genoring\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\external_entities\Entity\ExternalEntityTypeInterface;

/**
 * The Data Model Management Event.
 *
 * Given an external entity type corresponding to a data model, the event gather
 * form elements from event handlers to provide a complete form that can handle
 * actions on the data model.
 * typical types of actions:
 * - process one or more files to generate new ones
 * - process a file to generate a report
 * - convert a file into another format
 * - load data into a storage
 * - run a job of a tool
 * - backup data.
 */
class DataModelManagementEvent extends Event {

  /**
   * The data model.
   *
   * @var \Drupal\external_entities\Entity\ExternalEntityTypeInterface
   */
  protected $dataModel;

  /**
   * The management form.
   *
   * @var array
   */
  protected $managementForm;

  /**
   * Constructs a compatible data model event object.
   *
   * @param \Drupal\external_entities\Entity\ExternalEntityTypeInterface $data_model
   *   External entity type used as data model.
   */
  public function __construct(
    ExternalEntityTypeInterface $data_model,
  ) {
    $this->dataModel = $data_model;
    $this->managementForm = [];
  }

  /**
   * Gets the data model.
   *
   * @return \Drupal\external_entities\Entity\ExternalEntityTypeInterface
   *   The data model.
   */
  public function getDataModel() :ExternalEntityTypeInterface {
    return $this->dataModel;
  }

  /**
   * Gets the data management form.
   *
   * @return array
   *   The management form.
   */
  public function getManagementForm() :array {
    return $this->managementForm;
  }

  /**
   * Sets the data management form.
   *
   * @param array $management_form
   *   The new management form. It may/should contain submit buttons with custon
   *   submit handlers to handle actions. Event handlers setting the management
   *   form should get current managment form first and append/modify elements
   *   to/from the existing form.
   *
   * @return self
   *   Current instance.
   */
  public function setManagementForm(array $management_form) :self {
    $this->managementForm = $management_form;
    return $this;
  }

}
