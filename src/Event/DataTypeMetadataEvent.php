<?php

namespace Drupal\genoring\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * The Type Metadata Event.
 *
 * Provides a way to gather metadata fields that are related to a given file
 * type.
 */
class DataTypeMetadataEvent extends Event {

  /**
   * The machine name of the type of data.
   *
   * @var string
   */
  protected $dataType;

  /**
   * The category of the type of data.
   *
   * @var string
   */
  protected $category;

  /**
   * The metadata field structure with field form elements.
   *
   * @var array
   */
  protected $metadataFields;

  /**
   * Constructs a compatible data model event object.
   *
   * @param string $type
   *   The machine name of the type of data.
   * @param string $category
   *   The category of the type of data. Currently, only 'file' is used but
   *   other categories may be added in the future (eg. 'data_model').
   */
  public function __construct(string $type, string $category) {
    $this->dataType = $type;
    $this->category = $category;
    $this->metadataFields = [];
  }

  /**
   * Gets the type of data.
   *
   * @return string
   *   The type of data.
   */
  public function getDataType() :array {
    return $this->dataType;
  }

  /**
   * Gets the category of the type of data.
   *
   * @return string
   *   The category.
   */
  public function getCategory() :string {
    return $this->category;
  }

  /**
   * Gets the list of metadata fields.
   *
   * @return array
   *   A list of metadata fields.
   */
  public function getMetadataFields() :array {
    return $this->metadataFields;
  }

  /**
   * Add a metadata field.
   *
   * @param string $field_id
   *   A field identifier for the form element.
   * @param array $field_definition
   *   The field element definition.
   *
   * @return self
   *   Current instance.
   */
  public function addMetadataField(string $field_id, array $field_definition) :self {
    $this->metadataFields[$field_id] = $field_definition;
    return $this;
  }

}
